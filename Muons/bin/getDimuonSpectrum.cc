#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Photon.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include "Math/VectorUtil.h"

#include <protodarwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Muon {

////////////////////////////////////////////////////////////////////////////////
/// Obtain dimuon spectrum
void getDimuonSpectrum 
       (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
        const fs::path& output, //!< output ROOT file (n-tuple)
        const int steering, //!< parameters obtained from explicit options 
        const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    auto isMC = metainfo.Get<bool>("flags", "isMC");

    Event * ev = nullptr;
    tIn->SetBranchAddress("event", &ev);

    vector<RecMuon> * recMuons = nullptr;
    tIn->SetBranchAddress("recMuons", &recMuons);
    vector<RecPhoton> * recPhotons = nullptr;
    if (branchExists(tIn, "recPhotons"))
        tIn->SetBranchAddress("recPhotons", &recPhotons);

    int nbins = 100;
    double M = 3000, m = 30;
    float R = pow(M/m,1./nbins);
    vector<float> edges;
    edges.reserve(nbins+1);
    for (float edge = m; edge <= M; edge *= R) edges.push_back(edge);
    unique_ptr<TH1F> h_dimuon = make_unique<TH1F>("dimuon", "", edges.size()-1, edges.data());
    unique_ptr<TH1F> h_mmg = make_unique<TH1F>("mumugamma", "", edges.size()-1, edges.data());

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        if (recMuons->size() < 2) continue;

        FourVector m0 = recMuons->at(0).p4,
                   m1 = recMuons->at(1).p4;
        m0.SetPt(m0.Pt() * recMuons->at(0).scales.front() );
        m1.SetPt(m1.Pt() * recMuons->at(1).scales.front() );
        FourVector diMuon = m0 + m1;
        float w = ev->recWgts.front()
                * recMuons->at(0).weights.front()
                * recMuons->at(1).weights.front();
        if (isMC) w *= ev->genWgts.front();
        h_dimuon->Fill(diMuon.M(), w);

        if (recPhotons == nullptr || recPhotons->empty()) continue;

        FourVector ph = recPhotons->front().CorrP4();
        FourVector mumugamma = diMuon + ph;
        if (recPhotons->front().weights.size() > 0)
            w *= recPhotons->front().weights.front();
        h_mmg->Fill(mumugamma.M(), w);
    }

    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();
    h_dimuon->SetDirectory(fOut.get());
    h_mmg->SetDirectory(fOut.get());
    h_dimuon->Write();
    h_mmg->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of namespace DAS::Muon

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Obtain the dimuon mass spectrum.", DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");
        options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Muon::getDimuonSpectrum(inputs, output, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
