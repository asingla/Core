# BrilCalc

Important note: we have only managed to run at CERN so far (although in principle, it should be feasible to run it with tunnelling).

To set it up, follow [official instructions](https://cms-service-lumi.web.cern.ch/cms-service-lumi/brilwsdoc.html).

Once this is done, after compilation of the framework, the script can be used directly from the shell prompt as a command.
