#include <cassert>
#include <iostream>
#include <limits>
#include <cstdlib>
#include <vector>
#include <map>
#include <experimental/filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>

#include "Math/VectorUtil.h"
#include "TriggerEff.h"

#include "applyDataNormalisation.h"
#include "Core/Prefiring/interface/applyPrefiringWeights.h"

using namespace std;
using namespace experimental::filesystem;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Application of prefiring weights in data or simulation.
///
/// Calls a functor which apply the prefiring weights and then normalise with prescales
void applyPrefPrescales 
             (const fs::path& input,  //!< name of input root file 
              const fs::path& output, //!< name of output root file
              PrefOpt prefOpt, //!< option to choose the right map(s)
              const fs::path& lumi_file,      //!< path to text file with effective luminosities
              const fs::path& turnon_file,    //!< path to text file with turn-on points
              const fs::path& trigger_curves, //!< file with efficiency curves
              string& strategy,               //!< 'pt' or 'eta', only 'pt' has been tested for this command
                                              //!< (see the documentation of `Normalisation/bin/applyDataNormalisation.h`
              string& method,                 //!< 'presc'-> use the prescales in dataset, 'lumi'-> not implemented in this context
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    // Get old file, old tree and set top branch address
    assert(fs::exists(input));
    TChain * oldchain = new TChain("inclusive_jets"); // arg = path inside the ntuple
    oldchain->Add(input.c_str());

    Event * evnt = nullptr;
    oldchain->SetBranchAddress("event", &evnt);
    vector<RecJet> * recJets = nullptr;
    oldchain->SetBranchAddress("recJets", &recJets);
    vector<FourVector> * hltJets;
    oldchain->SetBranchAddress("hltJets", &hltJets);
    Trigger * trigger = nullptr;
    oldchain->SetBranchAddress("trigger", &trigger);

    if (fs::exists(output))
        cerr << red << output << " will be overwritten\n" << normal;
    TFile * newfile = TFile::Open(output.c_str(), "RECREATE");
    TTree * newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("PrefiringWeights");
    metainfo.AddCorrection("normalised");
    metainfo.AddRecEvWgt("Prefup");
    metainfo.AddRecEvWgt("Prefdown");
    bool isMC = metainfo.isMC();

    vector<GenJet> * genJets = nullptr;
    if (isMC) oldchain->SetBranchAddress("genJets", &genJets);

    int year = metainfo.year();
    assert(year > 2015 && year < 2019);

    //Calling functor to apply prefiring weights
    applyPrefiringWeightsFunctor apply(year, prefOpt, isMC);
    
    //Define control plots for prefiring weights
    ControlPlots::isMC = isMC;
    ControlPlots raw("raw");
    vector<ControlPlots> calib { ControlPlots("nominal_befNorm"),
                                 ControlPlots("upper_befNorm"  ),
                                 ControlPlots("lower_befNorm"  ) };
    auto totRecWgt = [&](size_t i) {
        return (isMC ? evnt->genWgts.front() : 1) * evnt->recWgts.at(i);
    };

    //Calling functor to apply data prescales
    DataNormalisation normalisation (lumi_file, turnon_file, trigger_curves, strategy, method, year);
    newfile->cd();

    //Define control plots for normalisation
    ControlPlots::isMC = false;
    ControlPlots corrNoTrigEff("corrNoTrigEff");

    vector<ControlPlots> corr ;
    for (int i=0; i<metainfo.GetNRecEvWgts(); i++)
        corr.push_back(ControlPlots(metainfo.GetRecEvWgt(i)));

    cout << "looping over events:" << endl;

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {
        //apply prefiring weights and fill their control plots
        if (isMC) raw(*genJets, evnt->genWgts.front());
        raw(*recJets, totRecWgt(0));
        apply(*evnt, *recJets);
        for (size_t i = 0; i < calib.size(); ++i) {
            if (isMC) calib.at(i)(*genJets, evnt->genWgts.front());
            if (recJets->size() == 0) continue;
            calib.at(i)(*recJets, totRecWgt(i));
        }
        //apply data prescales and fill their control plots
        auto leadingJet = normalisation(*evnt, *recJets, *hltJets, *trigger);
        if (evnt->recWgts.front()>0)
            newtree->Fill();
        if (normalisation.eff(leadingJet)>0)
            corrNoTrigEff(*recJets, evnt->recWgts.front()*normalisation.eff(leadingJet));
        if (normalisation.eff(leadingJet)>0) {
            for (size_t i = 0; i < corr.size(); ++i)
                corr.at(i)(*recJets, evnt->recWgts.at(i));
        }

        // Exclusive curves are filled (i.e. one event can populate only a trigger curve).
        // The ibit value is determined by the leading jet but also the other jets of the
        // event can populate the trigger curve.
        if (normalisation.eff(leadingJet) > 0) {
            for (auto& jet: *recJets) {
                auto y = abs(jet.Rapidity());
                auto w = evnt->recWgts.front();
                normalisation.eff.contribs.at(normalisation.ibit)->Fill(jet.CorrPt(), y, w);
            }
        }

        //cout << normalisation.eff.contribs.at(ibit)->GetTitle() << ' ' << it->second.turnon << ' ' << leading_pt << endl;
        
        
    }

    cout << "saving" << endl;
    newtree->AutoSave();
    corrNoTrigEff.Write(newfile);
    for (size_t i = 0; i < corr.size(); ++i)
        corr.at(i).Write(newfile);
    TDirectory * controlplots = newfile->mkdir("controlplots");
    controlplots->cd();
    for (TH2 * h: normalisation.eff.contribs) {
        h->SetDirectory(controlplots);
        h->Write();   
    }
    if (nNow == 0) {
            normalisation.eff.h->SetDirectory(controlplots);
        normalisation.eff.h->Write();
    }

    raw.Write(newfile);
    for (size_t i = 0; i < calib.size(); ++i)
        calib.at(i).Write(newfile);
    TDirectory * controlplotsPref = newfile->mkdir("Prefiring");
    apply.Write(controlplotsPref);

    newfile->Close();
    delete oldchain;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 6) {
        cout << argv[0] << " input output option [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = modified n-tuple\n"
             << "\t     \toption = `AverageMap` (default; only possible option for MC), `MapsPerEra`, or `SmoothMapsPerEra`\n"
             << "     \tlumi_file = 2-column file with 'trigger lumi' (should be in `$DAS_WORKAREA/tables/lumi`)\n"
             << "     \tturnon_file = 2-column file with 'trigger turn-on' (output of `getTriggerTurnons`, should also be in `$DAS_WORKAREA/tables/triggers`)\n"
             << "     \ttrigger_turnons = root file with output of `getTriggerTurnons`"
             << "     \tstrategy = 'pt' -> leading pt jet in the tracker acceptance is used to trigger the event and calculate the effciency\n"
             << "     \t           'eta'-> Not implemented in this context\n"
             << "     \t           (see `$DAS_SOFTWARE/Core/Normalisation/bin/applyDataNormalisation.h`)\n"
             << "     \tmethod = 'presc' -> use the prescales, 'lumi' -> not implemented in this context"
             << endl;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
             output = argv[2];

    TString option = argv[3];
    PrefOpt prefOpt = option == "MapsPerEra"       ?   PrefOpt::kMapsPerEra       :
                      option == "SmoothMapsPerEra" ?   PrefOpt::kSmoothMapsPerEra :
                    /*option == "AverageMap"       ?*/ PrefOpt::kAverageMap       ;
    
    fs::path lumi_file = argv[4],
             turnon_file = argv[5],
             trigger_curves = argv[6];
    string strategy = argv[7];
    string method = argv[8];
    int nNow = 0, nSplit = 1;
    if ( strategy == string("eta") ) {
        cerr << "strategy 'eta' has an undefined behaviour in this context\n";
        return EXIT_FAILURE;
    }
    if ( method == string("lumi") ) {
        cerr << "method 'lumi' has an undifined behaviour in this context\n";
        return EXIT_FAILURE;
    }
    if (argc > 9) nSplit = atoi(argv[9]);
    if (argc > 10) nNow = atoi(argv[10]);

    applyPrefPrescales(input, output, prefOpt, lumi_file, turnon_file, trigger_curves, strategy, method, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
