#include <cstdlib>
#include <string>
#include <iostream>
#include <filesystem>

#include <TFile.h>
#include <TROOT.h>
#include <TH2.h>
#include <TF1.h>
#include <TMath.h>

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/variables.h"

#include "Core/JEC/interface/Scale.h"
#include "common.h"

#include <protodarwin.h>

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Obtain JES corrections (L1 FastJet) versus pt, eta and rho (also for a given jet area A)
void getJEScurvesOffset
            (const fs::path input, //!< directory to JetMET JES tables
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering //!< steering parameters from `DT::Options`
            )
{
    unique_ptr<TFile> fOut(DT_GetOutput(output));

    cout << input.c_str() << endl;
    auto R    = config.get<int>("flags.R");
    auto year = config.get<int>("flags.year");
    cout << R << setw(10) << year << endl;
    JetEnergy::Scale jes(input.c_str(), R);
    float radius = R * 0.1; // Passing actual R value for the evaluation of jet area pi*R^{2}
    static double area = M_PI * pow(radius, 2); // Jet area estimation

    // Estimate correction at rho = rhoBinEdge_value, skipping the 999 value (last bin edge)
    for (int rho = 0; rho < nRhoBins.at(year); ++rho) {
        TString name  = Form("L1rho%d", rho),
                title = Form("#rho = %.2f, A = %.2f", rho_edges.at(year).at(rho), area);
        TH2 * h = new TH2D(name, title, nPtJERCbins, pt_JERC_edges.data(), nAbsEtaBins, abseta_edges.data());

        for (int ibin = 1; ibin <= h->GetNbinsX(); ++ibin)
        for (int jbin = 1; jbin <= h->GetNbinsY(); ++jbin) {
            double pt  = h->GetXaxis()->GetBinCenter(ibin),
                   eta = h->GetYaxis()->GetBinCenter(jbin);

            double corr = jes.GetL1Fast(pt, eta, area, rho_edges.at(year).at(rho));
            h->SetBinContent(ibin, jbin, corr);
        }
        h->Write();
    }
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        fs::path input, output;

        DT::Options options("Obtain JES corrections (L1 Offset) from JetMET tables versus pt, eta and rho. Correction depends on jet area");
        options.input   ("input" , &input      , "directory to tables provided by JetMET (in txt format)")
               .output  ("output", &output     , "output ROOT file")
               .arg<int>("R"     , "flags.R"   , "R parameter in jet clustering algorithm (x10)")
               .arg<int>("year"  , "flags.year", "year (4 digits)");

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::JetEnergy::getJEScurvesOffset(input, output, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

