#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <string>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include <TString.h>
#include <TFile.h>
#include <TH3.h>
#include <TH2.h>
#include <TH1.h>

#include "Math/VectorUtil.h"

#include "Core/CommonTools/interface/Greta.h"

#include "convolution.h"

#include "Core/JEC/interface/JESreader.h"

using namespace std;
using namespace DAS;

static const vector<double> Ms {3103, 2787, 2500, 1784, 1500};
static const double m = /*74*/ 97;

pair<int, int> GetRange (TH1 * h, int y)
{
    int im = h->FindBin(m),
        iM = h->FindBin(Ms.at(y-1)-1);
    //while (im < iM && h->GetBinContent(im) < deps) ++im; // pb: histogram is filled also at lower values by non-leading jets
    while (im < iM && h->GetBinContent(iM) < deps) --iM;
    return {im, iM};
}

TH3 * GetTH3 (TString input, TString name)
{
    TFile * source = TFile::Open(input, "READ");
    TH3 * h = dynamic_cast<TH3 *>(source->Get(name));
    assert(h != nullptr);
    h->SetDirectory(0);
    source->Close();
    return h;
}

JESreader GetJES (int R)
{
    string DAS = getenv("DAS_WORKAREA"),
           tag = "Summer16_07Aug2017_V11",
           dir = DAS + "/tables/JES/" + tag + "/" + tag + "_MC",
           smoothCorr = DAS + "/tables/JES/smooth/ak" + to_string(R) + "/" + tag + ".txt";
    return JESreader(dir, tag + "_MC", R/10., smoothCorr);
}

TH1 * GetSmoothHisto (TH1 * hIn, TF1 * f, int im, int iM)
{
    TH1 * hOut = dynamic_cast<TH1*>(hIn->Clone("s"));
    TAxis * axis = hIn->GetXaxis();
    for(int ipt=im; ipt <= iM; ++ipt) {
        double pt1 = axis->GetBinLowEdge(ipt),
               pt2 = axis->GetBinUpEdge(ipt);

        double ww = f->Integral(pt1, pt2, deps);
        //cout << ww << ' ' << integral(f,pt1,pt2) << endl;
        hOut->SetBinContent(ipt,ww);
    }
    return hOut;
}

////////////////////////////////////////////////////////////////////////////////
/// Make smooth fit and apply JES corrections in order to measure the 
/// amplitude of the wiggles
void getJESwiggles 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              int R) //!< jet radius
{
    TH3 * h3 = GetTH3(input, "nomBinning");

    TFile * file = TFile::Open(output, "RECREATE");

    JESreader jes = GetJES(R);

    auto corr = [&](double eta) {
        return [&](double pt) {
            return jes.GetSmoothL2Relative(pt, eta);
        };
    };

    for (int y = 1; y <= nYbins; ++y) { 

        cout << yBins.at(y-1) << endl;

        file->cd();
        TDirectory * p = file->mkdir(Form("ybin%d", y));
        p->cd();

        h3->GetYaxis()->SetRange(y,y);
        TH2 * h2 = dynamic_cast<TH2*>(h3->Project3D("zx"));
        h2->Print("base");
        h2->SetDirectory(p);
        h2->Write("h2");

        TH1 * h1 = h2->ProjectionX("h1"); 
        h1->SetTitle(yBins.at(y-1));
        h1->SetDirectory(p);
        h1->Write("h1");

        TH1 * sum_c = dynamic_cast<TH1 *>(h1->Clone("sum_c"));
        sum_c->Reset();
        sum_c->SetDirectory(0);

        TH1 * sum_s = dynamic_cast<TH1 *>(h1->Clone("sum_s"));
        sum_s->Reset();
        sum_s->SetDirectory(0);

        for (int ieta = 1, jeta = 1; jeta <= nEtaBins; ++jeta) {

            p->cd();

            // parameters for the current bin
            double mineta = h2->GetYaxis()->GetBinLowEdge(jeta),
                   maxeta = h2->GetYaxis()->GetBinLowEdge(jeta+1);
            const char sign = mineta > 0 ? 'p' : 'm';
            double eta = (mineta + maxeta)/2.;
            int ibin = round(abs(10*eta));
            TString name = Form("y%d_eta%c%d", y, sign, ibin);
            TString title = Form("%.2f < #eta < %.2f", mineta, maxeta);

            // projection raw histogram
            TH1 * h1 = h2->ProjectionX("h_" + name, jeta, jeta);
            double integral = h1->Integral();
            if (integral < deps) continue;
            cout << title << ' ' << integral << endl;
            h1->SetTitle(title);

            // creating directory if non-trivial
            TDirectory * pp = p->mkdir(Form("ieta%d", ieta)); // note: not the same convention as for the smoothed shifts
            ++ieta;
            h1->SetDirectory(pp);
            pp->cd();
            h1->Write("h");

            // getting range
            int im, iM;
            tie(im, iM) = GetRange(h1,y);
            if (im == iM) {
                cout << "(almost) empty" << endl;
                continue;
            }
            double M = h1->GetBinLowEdge(iM+1);

            // fitting
            h1->Scale(1,"width");
            TF1 * f = GetSmoothFit<6>(h1, im, iM, true); 
            if (f == nullptr) continue;
            f->SetTitle(title);
            f->Write("f");

            // getting smooth version of histogram before correction
            TH1 * s = GetSmoothHisto(h1, f, im, iM);
            s->SetDirectory(pp);
            s->SetTitle(title);
            s->Write("s");

            cout << "Convolving" << endl;
            Convolution<2> convolution(f, corr(eta));
            TF1 * f_conv = new TF1("convolution", convolution, m, M, 0); 

            // corrected version
            TH1 * c = dynamic_cast<TH1*>(h1->Clone("c_" + name));
            c->Reset();
            TAxis * axis = c->GetXaxis();
            for (int ipt=im; ipt < iM; ++ipt) {
                double pt1 = axis->GetBinLowEdge(ipt),
                       pt2 = axis->GetBinUpEdge(ipt);

                double w = f_conv->Integral(pt1, pt2, deps);
                c->SetBinContent(ipt, w);
            }
            c->Write("c");

            sum_s->Add(s);
            sum_c->Add(c);
        }
        p->cd();
        sum_s->SetDirectory(p);
        sum_s->Write("s");
        sum_c->SetDirectory(p);
        sum_c->Write("c");
    }

    file->Close();
}

////////////////////////////////////////////////////////////////////////////////
/// Make smooth fit and apply JES uncertainties in order to get smooth
/// uncertainty band
void getSmoothUnc
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              int R) //!< jet radius
{
    //TH3 * h3 = GetTH3(input, "uncBinning");
    //JESreader jes = GetJES(R);

    //TFile * file = TFile::Open(output, "RECREATE");

    //auto corr = [&](double eta) {
    //    return [&](double pt) {
    //        return jes.GetSmoothL2Relative(pt, eta);
    //    };
    //};
    //
    //for (int y = 1; y <= nYbins; ++y) { 

    //    cout << yBins.at(y-1) << endl;

    //    file->cd();
    //    TDirectory * p = file->mkdir(Form("ybin%d", y));
    //    p->cd();

    //    h3->GetYaxis()->SetRange(y,y);
    //    TH2 * h2 = dynamic_cast<TH2*>(h3->Project3D("zx"));
    //    h2->Print("base");
    //    h2->SetDirectory(p);
    //    h2->Write("h2");

    //    TH1 * sum = h2->ProjectionX("s"); 
    //    sum->Write("h1");
    //    sum->SetTitle(yBins.at(y-1));
    //    sum->Reset();
    //    sum->SetDirectory(0);

    //    for (int ieta = 1; ieta <= nEtaUncBins; ++ieta) {

    //        p->cd();

    //        double mineta = h2->GetYaxis()->GetBinLowEdge(ieta),
    //               maxeta = h2->GetYaxis()->GetBinLowEdge(ieta+1);
    //        const char sign = mineta > 0 ? 'p' : 'm';
    //        double eta = (mineta + maxeta)/2.;
    //        int ibin = round(abs(10*eta));
    //        TString name = Form("y%d_eta%c%d", y, sign, ibin);
    //        TString title = Form("%.2f < #eta < %.2f", mineta, maxeta);

    //        TH1 * h1 = h2->ProjectionX("h_" + name, ieta, ieta);
    //        double integral = h1->Integral();
    //        if (integral < eps) continue;
    //        cout << title << ' ' << integral << endl;
    //        h1->SetTitle(title);

    //        TDirectory * pp = p->mkdir(Form("ieta%d", ieta)); // note: not the same convention as for the smoothed shifts
    //        h1->SetDirectory(pp);
    //        pp->cd();
    //        h1->Write("h");

    //        int im, iM;
    //        tie(im, iM) = GetRange(h1,y);
    //        if (im == iM) {
    //            cout << "(almost) empty" << endl;
    //            continue;
    //        }
    //        double m = h1->GetBinLowEdge(im),
    //               M = h1->GetBinLowEdge(iM+1);

    //        h1->Scale(1,"width");
    //        TF1 * f = GetSmoothFit(h1, im, iM, 6); 
    //        if (f == nullptr) continue;
    //        f->SetTitle(title);
    //        f->Write("f");

    //        cout << "Convolving" << endl;
    //        Convolution<2> convolution(f, corr(eta));

    //        TF1 * f_conv = new TF1(Form("conv%d",y), convolution, m, M, 0); 

    //        TH1 * s1 = dynamic_cast<TH1*>(h1->Clone("s_" + name));
    //        s1->Reset();
    //        TAxis * axis = s1->GetXaxis();
    //        for (int ipt=im; ipt <= iM; ++ipt) {
    //            double pt1 = axis->GetBinLowEdge(ipt),
    //                   pt2 = axis->GetBinUpEdge(ipt);

    //            double w = f_conv->Integral(pt1, pt2, eps);
    //            s1->SetBinContent(ipt, w);
    //        }
    //        s1->Write("s");

    //        sum->Add(s1);
    //    }
    //    p->cd();
    //    sum->SetDirectory(p);
    //    sum->Write("c"); // c(orrected)
    //}

    //file->Close();
}


#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();

    if (argc < 4) {
        cout << argv[0] << " input outputNom outputUnc R\n"
             << "where\tinput = input root file with raw MC histograms (in `controlplots/pt_y`)\n"
             << "     \toutput = root file with \"corrected\" histogram and smooth fits of raw histograms\n"
             //<< "     \toutputUnc = root file with uncertainties and smooth fits (under development)\n"
             << "     \tR = jet radius (4 or 8)\n"
             //<< "NB: 2 outputs, because the eta binnings are different for the nominal correction and for the uncertainties\n"
             //<< "    Moreover, there are many many histograms... better keep them separate...\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
            //outputUnc = argv[3];

    int R = atoi(argv[3]);

    getJESwiggles(input, output, R);
    //getSmoothUnc(input, outputUnc, R);

    return EXIT_SUCCESS;
}
#endif
