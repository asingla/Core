#include <cstdlib>
#include <cassert>
#include <functional>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TProfile.h>
#include <TRandom.h>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Format.h"

#include "Core/CommonTools/interface/toolbox.h"

#include "Core/JEC/interface/Scale.h"
#include "common.h"
#include "getPtBalance.h"

#include "Math/VectorUtil.h"

#include <protodarwin.h>

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

template<typename Jet> const vector<double> JetEnergy::Plots<Jet>::y_edges = DAS::y_edges;

////////////////////////////////////////////////////////////////////////////////
/// Get Pt balance
void getPtBalance
            (const vector<fs::path> inputs, //!< input ROOT files (n-tuples)
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");

    Event * ev = nullptr;
    tIn->SetBranchAddress("event", &ev);
    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    tIn->SetBranchAddress("recJets", &recjets);

    vector<TString> var {"nominal"}; // TODO: rather fetch from metainfo
    if (isMC) {
        tIn->SetBranchAddress("genJets", &genjets);
        var.push_back("JER" + SysUp);
        var.push_back("JER" + SysDown);
    }
    else {
        const auto& JES = JetEnergy::Scale::uncs;
        var.insert(var.end(), JES.begin(), JES.end());
    }

    static const int N = var.size();

    // detector level
    vector<JetEnergy::Plots<RecJet>> plots;
    plots.push_back(JetEnergy::Plots<RecJet>("rec_any_vs_any", N, [](const RecJet& tag, const RecJet& probe){ return true ;}  ));

    // hadron level
    vector<JetEnergy::Plots<GenJet>> gplots;
    if (isMC) 
        gplots.push_back(JetEnergy::Plots<GenJet>("gen_any_vs_any", N, [](const GenJet& tag, const GenJet& probe) { return true; }));

    TRandom3 r3(metainfo.Seed<39856>(slice));
    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        auto gw = ev->genWgts.front(),
             rw = ev->recWgts.front();

        // we take the probe and the tag randomly
        auto r = r3.Uniform();
        int itag = (r<0.5),
            iprobe = (r>=0.5);

        // selection on pseudorapity
        if (JetEnergy::DijetSelection<RecJet>(recjets)
                && std::abs(recjets->at(itag).p4.Eta()) < 1.3)
        for (auto p: plots) {

            if (!p.condition(recjets->at(itag), recjets->at(iprobe))) continue;

            auto absy_probe = recjets->at(iprobe).AbsRap();
            for (int i = 0; i < N; ++i ) {

                auto pt_tag   = recjets->at(itag  ).CorrPt(i);
                auto pt_probe = recjets->at(iprobe).CorrPt(i);

                auto ptMean = (pt_tag+pt_probe)/2.0;
                auto bal = (pt_probe-pt_tag)/ptMean;

                p.hs[i]->Fill(bal, ptMean, absy_probe, gw*rw);
            }
        }

        /**************** GEN LEVEL ***********/
        if (!isMC) continue;
        if (!JetEnergy::DijetSelection<GenJet>(genjets)) continue;
        if (std::abs(genjets->at(itag).p4.Eta()) > 1.3) continue;

        for (auto p: gplots) {

            if (!p.condition(genjets->at(itag), genjets->at(iprobe))) continue;

            auto absy_probe = genjets->at(iprobe).AbsRap();

            for (int i = 0; i < N; ++i ) {
                auto pt_tag   = genjets->at(itag  ).p4.Pt();
                auto pt_probe = genjets->at(iprobe).p4.Pt();

                auto ptMean = (pt_tag+pt_probe)/2.0;
                auto bal = (pt_probe-pt_tag)/ptMean;

                p.hs[i]->Fill(bal, ptMean, absy_probe, gw);
            }
        }
    }

    for (auto p: plots)
        p.Write(fOut.get());

    if (isMC)
    for (auto p: gplots)
        p.Write(fOut.get());

    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Get pt balance in bins of pt and eta.", DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetEnergy::getPtBalance(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
}
#endif
