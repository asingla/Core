#include <functional>
#include <TF1.h>

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Convolution
///
// TODO: explain a bit more :D
template <int order> struct Convolution {

    TF1 * f; //!< "raw" pt spectrum
    function<double(double)> corr; //!< correction as a function of the raw pt

    Convolution (TF1 * F, function<double(double)> Corr) : f(F), corr(Corr) {}

    // TODO: Greta fit in the constructor in order to avoid the slow TF1 object

    double operator() (double *x, double *p)
    {
        double corrpt = *x;

        if (corrpt == 0.) {
            cout << __LINE__ << ' ' << corrpt << endl;
            exit(EXIT_FAILURE);
        }

        double rawpt = corrpt/corr(corrpt);
        for (int i = 1; i < order; ++i) rawpt = corrpt/corr(rawpt);

        if (rawpt == 0.) {
            cout << __LINE__ << ' ' << corrpt << ' ' << rawpt << endl;
            exit(EXIT_FAILURE);
        }

        double m1 = corr(rawpt),
               m2 = corr(rawpt-0.5),
               m0 = corr(rawpt+0.5);

        if (m1 == 0. && m2 == m0) {
            cout << __LINE__ << ' ' << corrpt << ' ' << rawpt << ' ' << m1 << ' ' << m2 << ' ' << m0 << endl;
            exit(EXIT_FAILURE);
        }

        double eval = f->Eval(rawpt);

        if (isnan(eval)) {
            cout << __LINE__ << ' ' << corrpt << ' ' << rawpt << ' ' << m1 << ' ' << m2 << ' ' << m0 << ' ' << eval << endl;
            exit(EXIT_FAILURE);
        }

        return f->Eval(rawpt) / (m1 + rawpt*(m2-m0));
    }
};
                //// convolution of pt spectrum and function describing the uncertainty as a function of *corrected* pt
                //auto conv = [&](double *x, double *p) {
                //    double corrpt =  *x;

                //    auto unc = [&](double rawpt) {
                //        uncSource->setJetEta(eta);
                //        uncSource->setJetPt(rawpt);
                //        return 1.-(1-2*sign)*uncSource->getUncertainty(sign);
                //    };
                //    double rawpt0 = corrpt/unc(corrpt);
                //    double rawpt = corrpt/unc(rawpt0);
                //    // NB: one could loop a few more times...

                //    // main term
                //    double u1 = unc(rawpt);

                //    // correction term in case unc is rapidly changing
                //    double u0 = unc(rawpt-0.5);
                //    double u2 = unc(rawpt+0.5);

                //    // here, f is a function of *raw* pt, w/o correction
                //    return f->Eval(rawpt) / (u1 + rawpt*(u2-u0));
                //};
}
