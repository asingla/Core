#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/JEC/interface/JESreader.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>

#include "Math/VectorUtil.h"

#include "common.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

static const vector<double> JECL5Ybins {-5.191,-3.839,-3.489,-3.139,-2.964,-2.853,-2.65,-2.5,-2.322,-2.172,-1.93,-1.653,-1.479,-1.305,-1.044,-0.783,-0.522,-0.261,0,0.261,0.522,0.783,1.044,1.305,1.479,1.653,1.93,2.172,2.322,2.5,2.65,2.853,2.964,3.139,3.489,3.839 ,5.191};
static const int nJECL5Ybins = JECL5Ybins.size()-1;

////////////////////////////////////////////////////////////////////////////////
/// Template for function
void getJEScurvesFlavour 
             (const char * input,  //!< name of input root file 
              const char * output) //!< name of output root file
{
    TFile * file = TFile::Open(output, "RECREATE");

    vector<double> logpt_edges = GetLogBinning(20, 3200, 1000);
    int nlogptBins = logpt_edges.size()-1;
    TH2 * ud = new TH2D("ud", "ud", nlogptBins, logpt_edges.data(), nJECL5Ybins, JECL5Ybins.data()), 
        * s  = new TH2D("s" , "s" , nlogptBins, logpt_edges.data(), nJECL5Ybins, JECL5Ybins.data()),
        * c  = new TH2D("c" , "c" , nlogptBins, logpt_edges.data(), nJECL5Ybins, JECL5Ybins.data()),
        * b  = new TH2D("b" , "b" , nlogptBins, logpt_edges.data(), nJECL5Ybins, JECL5Ybins.data()),
        * g  = new TH2D("g" , "g" , nlogptBins, logpt_edges.data(), nJECL5Ybins, JECL5Ybins.data());

    fs::path p = input;
    cout << p << endl;
    assert(fs::is_directory(p));
    cout << p.stem().c_str() << endl;

    JESreader * jecs = new JESreader(input, p.stem().c_str(), 0.4f);
    for (int ipt = 1; ipt <= nlogptBins; ++ipt)
    for (int ieta = 1; ieta <= nJECL5Ybins; ++ieta) {

        double pt  = ud->GetXaxis()->GetBinCenter(ipt),
               eta = ud->GetYaxis()->GetBinCenter(ieta);

        ud->SetBinContent(ipt,ieta,jecs->GetL5Flavor(pt, eta, 1));
        s ->SetBinContent(ipt,ieta,jecs->GetL5Flavor(pt, eta, 3));
        c ->SetBinContent(ipt,ieta,jecs->GetL5Flavor(pt, eta, 4));
        b ->SetBinContent(ipt,ieta,jecs->GetL5Flavor(pt, eta, 5));
        g ->SetBinContent(ipt,ieta,jecs->GetL5Flavor(pt, eta, 0));
    }

    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output" << endl;
        return EXIT_SUCCESS;
    }

    const char * input = argv[1],
               * output = argv[2];

    getJEScurvesFlavour(input, output);
    return EXIT_SUCCESS;
}
#endif

