# Trigger

## `getTriggerCurves`

Produces the trigger efficiency curves with the emulation method.
For the lower trigger, no method has been implemented so far.

## `getTriggerTurnons`

Fit the trigger curves with a sigmoid function and find the turn-on at 99% of
efficiency. (The turn-on point is rounded up to the next bin edge.)
