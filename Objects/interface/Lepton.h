#ifndef _Leptons_
#define _Leptons_

#include <vector>
#include <iostream>

#include "Core/CommonTools/interface/variables.h"

#include "Core/Objects/interface/Weight.h"

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// class GenMuon
///
/// Contains four-momentum 
struct GenMuon {
    static inline const char * const ScaleVar = "MuonGenScales", //< Name of muon gen scales in variations
                             * const WeightVar = "MuonGenWgts";  //< Name of muon gen weights in variations

    int Q; //!< +/- 1

    FourVector p4; //!< Four-momentum

    std::vector<Weight> weights; //!< muon weight (the int can be used to express non-trivial correlations)

    GenMuon (); //!< Constructor (trivial)

    float Rapidity () const; //!< return rapidity
    float AbsRap () const; //!< return absolute rapidity

    /// Returns the muon four-vector
    virtual FourVector CorrP4 (size_t i = 0) const { return p4; }

    /// Destructor
    virtual ~GenMuon() = default;
};

////////////////////////////////////////////////////////////////////////////////
/// class RecMuon
struct RecMuon : public GenMuon {
    static inline const char * const ScaleVar = "MuonScales",   //< Name of muon rec scales in variations
                             * const WeightVar = "MuonRecWgts"; //< Name of muon rec weights in variations

    unsigned int selectors; //!< [muon ID & PF isolation](https://github.com/cms-sw/cmssw/blob/CMSSW_9_4_X/DataFormats/MuonReco/interface/Muon.h#L188-L212)

    float Dxy, //!< transverse distance to PV
          Dz; //!< longitudinal distance to PV

    int nTkHits; //!< number of hits in tracker

    std::vector<float> scales; //!< e.g. Rochester corrections

    RecMuon (); //!< Constructor (trivial)

    float RawPt () const; //!< w/o energy correction
    float CorrPt          //!< w. energy correction
        (size_t i = 0) const; //!< index in scale vector

    FourVector CorrP4 (size_t i = 0) const;
};

/// Obtain the calibrated (data) or smeared (MC) 4-momentum for the given variation
/// of the corrections.
inline FourVector RecMuon::CorrP4 (size_t i) const
{
    FourVector corrected = p4;
    corrected.SetPt(CorrPt(i));
    return corrected;
}

}

#if defined(__ROOTCLING__)
#pragma link C++ class DAS::GenMuon +;
#pragma link C++ class std::vector<DAS::GenMuon> +;

#pragma link C++ class DAS::RecMuon +;
#pragma link C++ class std::vector<DAS::RecMuon> +;

#endif

#endif
