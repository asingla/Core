# Objects

Inclusive observables are multi-count, which means that in one event may consist of more than one entry.
Hence two groups of structures have been defined in `interface` (for the headers) and `src` (for the source).
 - Events, containing all information common to all entries, such as the pile-up or the MET.
 - Jets, containing all information relative to one single entry.

All structures contain a `clear()` method for easier use in the *n*-tupliser.

One particular feature related to the current framework is the presence of vector of *weights*.

## `Jet.*`

Three structures are defined to contain the information of a jet:

 - `GenJet`: only intended for simulation, consisting of a 4-vector and of the number of heavy hadrons.
 - `RecJet`: inteded for both data and simulation; the number of heavy hadrons only makes sense in simulation.
 - `deepcsv`: only defined to reduce the number of members in `RecJet`, it contains the probabilities entering the definition of the DeepCSV discriminate (NB: `probcc` is used in 2016 but not in 2017, where it is trivially defined to `-1`).

Two remarks regarding the type `FourVector`:
 - It is a typedef of an existing tag defined in the `CommonTools` subpackage.
 - For the HLT jets, since one only needs to know the kinematics, no dedicated structure is required, and one can simply use `FourVector`.

## `Event.*`

Five structures are defined to contain the information of the event.

 - `Event` for general information such as run number, or weight.
 - `Trigger` only contains the bits corresponding to the fired triggers (note that originally, it was also supposed to contain the prescale, but this is finally done using the effective luminosities).
 - `MET`
 - `PileUp`
 - `PrimaryVertex `

