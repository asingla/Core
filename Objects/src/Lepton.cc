#include "Core/Objects/interface/Lepton.h"

using namespace DAS;

GenMuon::GenMuon () :
    Q(0), weights{{1.,0}}
{}

RecMuon::RecMuon () :
    GenMuon(), selectors(0), Dxy(-1), Dz(-1), nTkHits(0), scales(1,1)
{}

float GenMuon::Rapidity () const {
    auto E = p4.E(), pz = p4.Pz();
    return 0.5*log((E+pz)/(E-pz));
}
float GenMuon::AbsRap () const { return fabs(Rapidity()); }

float RecMuon::RawPt () const { return p4.Pt(); }
float RecMuon::CorrPt (size_t i) const { return scales.at(i)*p4.Pt(); }

// TODO: float RecMuon::CorrE (size_t i) const; // preserve mass
