#include "Core/Objects/interface/Jet.h"
#include <cmath>

#define DUMMY -999

using namespace DAS;

GenJet::GenJet () :
    nBHadrons(DUMMY), nCHadrons(DUMMY), partonFlavour(DUMMY), weights(1)
{ }

float GenJet::Rapidity () const {
    auto E = p4.E(), pz = p4.Pz();
    return 0.5*log((E+pz)/(E-pz));
}
float GenJet::AbsRap () const { return fabs(Rapidity()); }


RecJet::RecJet () :
    GenJet(), area(DUMMY), DeepJet{DUMMY, /*DUMMY,*/ DUMMY, DUMMY, DUMMY, DUMMY, DUMMY}, JECs(1,1) {}

float RecJet::RawPt () const { return p4.Pt(); }
float RecJet::CorrPt (size_t i) const { return JECs.at(i)*p4.Pt(); }
