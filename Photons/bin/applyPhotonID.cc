#include <cstdlib>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Photon.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/GenericSFApplier.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TChain.h>
#include <TFile.h>

#include <protodarwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Photon {

/**
 * \brief Applies the photon ID selection and corrections (scale factors)
 *
 * In addition, vetoes photons in the barrel-endcap transition region
 * (1.44 < |scEta| < 1.57).
 *
 * This class modifies the weights of the reconstructed photons to apply the
 * efficiency correction. Scale factors are taken from a ROOT file in the
 * format provided by Egamma for UL scale factors.
 *
 * The scale factors are retrieved from a histogram called EGamma_SF2D assumed
 * to contain the scale factors in bins of supercluster eta and pT.
 */
class IDApplier : public GenericSFApplier<DAS::RecPhoton> {
    unsigned m_mask; ///< Mask to apply to the photon's selectors

    /// Gets the enum value for a photon ID name.
    static RecPhoton::Identification getMask (const string& ID)
    {
        if (ID == "Loose")
            return RecPhoton::CutBasedLoose;
        else if (ID == "Medium")
            return RecPhoton::CutBasedMedium;
        else if (ID == "Tight")
            return RecPhoton::CutBasedTight;
        else if (ID == "MVA80")
            return RecPhoton::MVAWorkingPoint80;
        else if (ID == "MVA90")
            return RecPhoton::MVAWorkingPoint90;

        BOOST_THROW_EXCEPTION( invalid_argument(ID + " is not recognised.") );
    }

public:
    IDApplier (const fs::path& filePath, const string& ID,
               bool correction, bool uncertainties)
        : GenericSFApplier(filePath, correction, uncertainties)
        , m_mask(getMask(ID))
    {
        loadNominal("EGamma_SF2D");
        loadBinWiseUnc("ID_statData",       "statData");
        loadBinWiseUnc("ID_statMC",         "statMC");
        loadGlobalUnc("ID_altBkgModel",     "altBkgModel");
        loadGlobalUnc("ID_altSignalModel",  "altSignalModel");
        loadGlobalUnc("ID_altMCEff",        "altMCEff");
        loadGlobalUnc("ID_altTagSelection", "altTagSelection");

        cout << "Initialisation of " << __func__ << " done" << endl;
    }

protected:
    bool passes (const RecPhoton& photon) const override
    {
        const bool passesID = (m_mask & photon.selectors) == m_mask;
        const auto absScEta = abs(photon.scEta);
        return passesID && (absScEta < 1.44 || absScEta > 1.57);
    }

    int binIndex (const RecPhoton& photon,
                  const std::unique_ptr<TH1>& hist) const override
    {
        // - 1 to ensure we fall within the bin
        const float maxpt = hist->GetYaxis()->GetXmax() - 1;
        return hist->FindBin(photon.scEta, min(photon.p4.Pt(), maxpt));
    }
};

////////////////////////////////////////////////////////////////////////////////
void applyPhotonID
       (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
        const fs::path& output, //!< output ROOT file (n-tuple)
        const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
        const int steering, //!< parameters obtained from explicit options
        const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);

    vector<RecPhoton> * recPhotons = nullptr;
    tIn->SetBranchAddress("recPhotons", &recPhotons);

    auto ID = config.get<string>("corrections.photons.ID");
    auto path = config.get<fs::path>("corrections.photons.IDtable");
    metainfo.Set<string>("corrections", "photons", "ID", ID);

    IDApplier applier(path, ID, path != "/dev/null", (steering & DT::syst) == DT::syst);
    for (const auto& name: applier.weightNames())
        metainfo.Set<string>("variations", RecPhoton::WeightVar, name);

    // TODO: ControlPlots

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        applier(*recPhotons);

        static const bool fillTree = (steering & DT::fill) == DT::fill;
        if (fillTree) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of namespace DAS::Photon

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Selects photons passing the ID criteria. Also "
                            "requires them not to fall in the EE-EB "
                            "transition region.",
                            DT::config | DT::split | DT::fill | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<string>("ID", "corrections.photons.ID",
                            "The photon ID to use (Loose/Medium/Tight/MVA80/MVA90)")
               .arg<fs::path>("IDtable", "corrections.photons.IDtable",
                              "The file from which ID scale factors are taken "
                              "(or /dev/null for no correction)");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Photon::applyPhotonID(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
