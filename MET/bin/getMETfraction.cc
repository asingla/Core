#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>
#include <limits>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TString.h>
#include <TFile.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"

#include <protodarwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

static const auto feps = numeric_limits<float>::epsilon();

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// TODO: reimplement with Boost Property Tree
/// TODO: provide turnons from the command line
vector<double> GetTriggerTurnons (int year, int R)
{
    ifstream file;
    fs::path p = getenv("DAS_WORKAREA");
    p /= Form("tables/triggers/%d/ak%d.txt", year, R);
    assert(fs::exists(p));
    file.open(p.c_str());

    vector<double> edges(1, 30);
    float HLT, PF;
    do {
        file >> HLT >> PF;
        if (PF > edges.back()) edges.push_back(PF);
    }
    while (file.good() && !file.fail());
    edges.push_back(6500);

    file.close();

    return edges;
}

////////////////////////////////////////////////////////////////////////////////
/// Get MET fraction for each jet trigger separately
void getMETfraction 
              (const vector<fs::path>& inputs, //!< input ROOT file (n-tuple)
               const fs::path& output, //!< output ROOT file (histograms)
               const int steering, //!< parameters obtained from explicit options 
               const DT::Slice slice = {1,0} //!< number and index of slice
               )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    auto year = metainfo.Get<int>("flags", "year");
    auto R = metainfo.Get<int>("flags", "R");

    Event * ev = nullptr;
    MET * met = nullptr;
    tIn->SetBranchAddress("event", &ev);
    tIn->SetBranchAddress("met", &met);
    
    vector<RecJet> * recJets = nullptr;
    tIn->SetBranchAddress("recJets", &recJets);

    vector<double> MET_edges;
    for (float edge = 0; edge <= 1; edge += 0.01)
        MET_edges.push_back(edge);
    int nMETbins = MET_edges.size()-1;

    vector<double> trig_edges = GetTriggerTurnons(year, R);
    int nTrigBins = trig_edges.size() - 1;

    auto METfraction = new TH2F("METfraction", ";MET fraction;p^{leading}_{T}   (GeV)",
                                    nMETbins, MET_edges.data(),
                                    nTrigBins, trig_edges.data());

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        if (recJets->empty()) continue;
        static auto rapAcc = y_edges.back();
        if (abs(recJets->front().Rapidity()) >= rapAcc) continue;
        if (abs(met->SumEt) < feps) continue;

        auto pt = recJets->front().CorrPt();
        auto frac = met->Et/met->SumEt;
        auto w = ev->genWgts.front() * ev->recWgts.front();

        //cout << setw(15) << pt << setw(15) << frac << setw(15) << w << endl;

        // we do not use the jet weight,
        // because the pt is only determined 
        // to find the right trigger window
        // -> only the event weight matters
        METfraction->Fill(frac, pt, w);
    }

    METfraction->Write();

    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Obtain MET fraction per trigger.", DT::split);

        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file"               );

        options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::getMETfraction(inputs, output, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
