#include <cstdlib>
#include <cassert>
#include <iostream>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include <TChain.h>
#include <TDirectory.h>
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>

#include "Math/VectorUtil.h"

#include "Core/MET/interface/Filters.h"

namespace DAS::MissingET {

struct FractionCut {

    const TString name;

    // from MET
    std::shared_ptr<TH1> Et, SumEt, Fraction, Pt, Phi;

    std::shared_ptr<TH2> pt_y;

    FractionCut (TString Name) : //!< Constructor, initialises all histograms
        name    (Name),
        Et      (std::make_shared<TH1F>(Name + "Et", "Et", 100, 1, 1000)),
        SumEt   (std::make_shared<TH1F>(Name + "SumEt", "SumEt", 650, 1, 6500)),
        Fraction(std::make_shared<TH1F>(Name + "Fraction", "Fraction", 100, 0, 1)),
        Pt      (std::make_shared<TH1F>(Name + "Pt", "Pt", 100, 1, 1000)),
        Phi     (std::make_shared<TH1F>(Name + "Phi", "Phi", 314, -M_PI, M_PI)),
        pt_y    (std::make_shared<TH2F>(Name + "pt_y", "pt_y", nPtBins, pt_edges.data(), nYbins, y_edges.data()))
    {
        cout << __func__ << ' ' << name << endl;
    }

    void Fill //!< Fill all histograms directly
        (Event * ev, //!< event info (run number, etc, but also weight)
         MET * met,  //!< MET info
         std::vector<RecJet> * recJets) //!< rec jets
    {
        float w = ev->recWgts.front();
        if (ev->genWgts.size() > 0) w *= ev->genWgts.front();

        // from MET
        Et->Fill(met->Et, w);
        SumEt->Fill(met->SumEt, w);
        Fraction->Fill(met->Et/met->SumEt, w);
        Pt->Fill(met->Pt, w);
        Phi->Fill(met->Phi, w);

        // jet
        for (const RecJet& jet: *recJets)  {
            float jW = jet.weights.front();
            pt_y->Fill(jet.CorrPt(), jet.AbsRap(), w*jW);
        }
    }

    void Write (TDirectory * d)
    {
        d->cd();
        TDirectory * dd = d->mkdir(name);
        dd->cd();
        for (auto& h: {Et, SumEt, Fraction, Pt, Phi}) {
            h->SetDirectory(d);
            TString n = h->GetName();
            n.ReplaceAll(name,"");
            h->Write(n);
        }
        pt_y->SetDirectory(d);
        TString n = pt_y->GetName();
        n.ReplaceAll(name,"");
        pt_y->Write(n);
        d->cd();
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Apply MET filters, both for MC and Data
/// following the official recommendations
void applyMETfilters 
              (const vector<fs::path>& inputs, //!< input ROOT file (n-tuple)
               const fs::path& output, //!< output ROOT file (n-tuple)
               const int steering, //!< parameters obtained from explicit options 
               const DT::Slice slice = {1,0} //!< number and index of slice
               )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    auto year = metainfo.Get<int>("flags", "year");

    MET * met = nullptr;
    Event * event = nullptr;
    vector<RecJet> * recJets = nullptr;
    tIn->SetBranchAddress("event", &event);
    tIn->SetBranchAddress("met", &met);
    tIn->SetBranchAddress("recJets", &recJets);

    metainfo.Set<bool>("corrections", "METfilters", true);
    MissingET::Filters metfilters(year);

    /**** declaring a few histograms to control effect of filters ****/

    MissingET::FractionCut beforeCut("beforeMETcut"),
                           afterCut("afterMETcut");

    ControlPlots METbefore("METbefore"),
                 METafterAllMETfilters("METafterAllMETfilters"),
                 METafterMETfraction("METafterMETfraction"),
                 METafterAllMETfiltersAndMETfraction("METafterAllMETfiltersAndMETfraction");

    vector<ControlPlots> METfilters;
    for (TString METname: metfilters.METnames)
        METfilters.push_back(ControlPlots(METname));

    auto totRecWgt = [&](size_t i) {
        return (isMC ? event->genWgts.front().v : 1) * event->recWgts.at(i).v;
    };

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        METbefore(*recJets, totRecWgt(0));

        beforeCut.Fill(event, met, recJets);

        if (met->Et < 0.3 * met->SumEt){ 
            afterCut.Fill(event, met, recJets);
            METafterMETfraction(*recJets, totRecWgt(0));
        }

        for (size_t ibit = 0; ibit < metfilters.METbitsToApply.size(); ++ibit) {
            bool bit = met->Bit.at(ibit);
            if (!bit) continue;
            METfilters.at(ibit)(*recJets, totRecWgt(0));
        }

        // IMPORTANT NOTE:
        // in case the MET fraction based cut would be used,
        // the description of the MET class should be changed!!
        
        metfilters(met, event, recJets);
        METafterAllMETfilters(*recJets, totRecWgt(0));
    
        if (met->Et < 0.3 * met->SumEt)
            METafterAllMETfiltersAndMETfraction(*recJets, totRecWgt(0));

        if ((steering & DT::fill) == DT::fill) tOut->Fill();
    }

    beforeCut.Write(fOut.get());
    afterCut.Write(fOut.get());
    METbefore.Write(fOut.get());
    METafterMETfraction.Write(fOut.get());
    METafterAllMETfilters.Write(fOut.get());
    METafterAllMETfiltersAndMETfraction.Write(fOut.get());

    for (auto& filt: METfilters)
        filt.Write(fOut.get());

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::MissingET namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Applies the MET filters by removing the rec-level jets.",
                            DT::split | DT::fill);

        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file"               );

        options(argc,argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::MissingET::applyMETfilters(inputs, output, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
