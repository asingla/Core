#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <string>
#include <filesystem>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "Core/Objects/interface/Event.h"

#include "Math/VectorUtil.h"

#include <protodarwin.h>

namespace DAS::PUprofile {

////////////////////////////////////////////////////////////////////////////////
/// Note: `pileup_latest.txt` is a JSON file with following structure:
/// ```
/// {
///     "run": [["LS", "inst lumi", "xsec RMS", "av xsec"]],
/// }
/// ```
/// The average number of pile-up can be estimated as follows:
/// ```latex
/// nPUmean = av xsec * MB xsec 
/// ```
class Latest {
    std::filesystem::path file;
    boost::property_tree::ptree json; //!< `pileup_latest.txt`

public:
    ////////////////////////////////////////////////////////////////////////////////
    /// The constructor just opens the JSON file and puts it into a Boost 
    /// Property Tree.
    Latest (const std::filesystem::path& pileup_file) :
        file(pileup_file)
    {
        using namespace std;
        namespace fs = filesystem;
        if (!fs::exists(file))
            BOOST_THROW_EXCEPTION(fs::filesystem_error("The pileup latest file could not be found",
                                  file, make_error_code(errc::no_such_file_or_directory)));

        namespace pt = boost::property_tree;                          
        pt::read_json(pileup_file.c_str(), json);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Finds value corresponding to the run and the lumi, and sets it in the event.
    void operator() (Event * event, PileUp * pu) const
    {
        using namespace std;

        auto runNo = event->runNo,
             lumi = event->lumi;

        auto runNo_str = to_string(runNo);
        auto run = json.get_child_optional(runNo_str);
        if (!run)
            BOOST_THROW_EXCEPTION(runtime_error(runNo_str + " could not be found in the "
                                                   + file.string() + " file"));

        //cout << runNo << ' ' << lumi << '\t'; // TODO: verbosity
        for (auto& LS: *run) {
            auto it = LS.second.begin();  // ["LS", "inst lumi", "xsec RMS", "av xsec"]
            auto LSno = it->second.get_value<int>(); // LS
            if (LSno != lumi) continue;
            ++it; //auto instLumi = it->second.get_value<float>(); // inst lumi
            ++it; //auto xsecRMS = it->second.get_value<float>(); // xsec RMS
            ++it; auto avXsec = it->second.get_value<float>(); // av xsec
            pu->trpu = avXsec * DAS::PileUp::MBxsec;
            //cout << instLumi << ' ' << avXsec << ' ' << ' ' << xsecRMS << '\t' << pu->trpu << endl; // TODO: verbosity
            break;
        }   
    }
};

}
