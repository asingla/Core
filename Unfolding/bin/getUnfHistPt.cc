#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Core/JEC/interface/matching.h"

#include <TH1.h>
#include <TH2.h>

#include "Math/VectorUtil.h"
#include "Math/GenVector/GenVector_exception.h"

#include <protodarwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Unfolding::InclusiveJet1D {

static const std::vector<double> recBins{74,84,97,114,133,153,174,196,220,245,272,300,330,362,395,430,468,507,548,592,638,686,737,790,846,905,967,1032,1101,1172,1248,1327,1410,1497,1588,1684,1784,1890,2000,2116,2238,2366,2500,2640,2787,2941,3103,3273,3450,3637,3832};
static const std::vector<double> genBins{74   ,97    ,133    ,174    ,220    ,272    ,330    ,395    ,468    ,548    ,638    ,737    ,846    ,967     ,1101     ,1248     ,1410     ,1588     ,1784     ,2000     ,2238     ,2500     ,2787     ,3103     ,3450,     3832};

static const int nRecBins = recBins.size()-1;
static const int nGenBins = genBins.size()-1;

static const double minpt = recBins.front(), maxpt = recBins.back(), maxy = 2.0;

////////////////////////////////////////////////////////////////////////////////
/// Holds the histograms and indices corresponding to one variation
struct Variation {
    const TString name;
    static bool isMC;
    const size_t iJEC, iGenJetWgt, iRecJetWgt,
                       iGenEvtWgt, iRecEvtWgt;
    TH1 * rec, * tmp, * gen,
        * missNoMatch, * missOut,
        * fakeNoMatch, * fakeOut;
    TH2 * cov, * RM;

    Variation (TString Name, size_t IJEC = 0,
            size_t IGenJetWgt = 0, size_t IRecJetWgt = 0,
            size_t IGenEvtWgt = 0, size_t IRecEvtWgt = 0) :
        name(Name), iJEC(IJEC),
        iGenJetWgt(IGenJetWgt), iRecJetWgt(IRecJetWgt),
        iGenEvtWgt(IGenEvtWgt), iRecEvtWgt(IRecEvtWgt),
        rec(nullptr), tmp(nullptr), gen(nullptr),
        missNoMatch(nullptr), missOut(nullptr),
        fakeNoMatch(nullptr), fakeOut(nullptr),
        cov(nullptr), RM(nullptr)
    {
        rec = new TH1D(Name + "rec", "detector level", nRecBins, recBins.data()),
        tmp = new TH1D(Name + "tmp", "", nRecBins, recBins.data());
        cov = new TH2D(Name + "cov", "covariance matrix of detector level", nRecBins, recBins.data(), nRecBins, recBins.data());
        if (!isMC) return;
        gen         = new TH1D(Name + "gen"        , "hadron level", nGenBins, genBins.data()),
        missNoMatch = new TH1D(Name + "missNoMatch", "real inefficiency"                                                 , nGenBins, genBins.data()),
        missOut     = new TH1D(Name + "missOut"    , "good gen jets matched to jets reconstructed out of the phase space", nGenBins, genBins.data()),
        fakeNoMatch = new TH1D(Name + "fakeNoMatch", "real background"                                               , nRecBins, recBins.data()),
        fakeOut     = new TH1D(Name + "fakeOut"    , "good rec jets matched to jets generated out of the phase space", nRecBins, recBins.data()),
        RM          = new TH2D(Name + "RM", "migrations of jets in phase space at both levels", nGenBins, genBins.data(), nRecBins, recBins.data());
        // TODO: investigate the migrations out of the phase space with the underflow and overflow
    }

    void Write (TFile * file)
    {
        cout << "Writing " << name << endl;
        file->cd();
        TDirectory * dir = file->mkdir(name);
        dir->cd();
#define PAIR(name) make_pair(dynamic_cast<TH1*>(name), #name)
        for (auto p: { PAIR(gen), PAIR(missNoMatch), PAIR(missOut),
                       PAIR(rec), PAIR(fakeNoMatch), PAIR(fakeOut),
                       PAIR(cov), PAIR(RM) }) {
#undef PAIR
            if (p.first == nullptr) continue;
            p.first->SetDirectory(dir);
            p.first->Write(p.second);
        }
    }
};
bool Variation::isMC = false; // just fort initialisation

////////////////////////////////////////////////////////////////////////////////
/// Get histograms for unfolding only for pt (in d=1), 
/// without using `TUnfoldBinning`.
///
/// Then the output can be unfolded using `unfold`.
void getUnfHistPt
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    bool isMC = metainfo.Get<bool>("flags", "isMC");

    // event information
    Event * ev = nullptr;
    tIn->SetBranchAddress("event", &ev);

    // jet information
    vector<RecJet> * recJets = nullptr;
    tIn->SetBranchAddress("recJets", &recJets);
    vector<GenJet> * genJets = nullptr;
    if (isMC)
        tIn->SetBranchAddress("genJets", &genJets);

    cout << "Setting variations" << endl;

    Variation::isMC = isMC;
    vector<Variation> variations { Variation("nominal") };
    if ((steering & DT::syst) == DT::syst) {
        variations.reserve(100); // TODO?

        TList * ev_wgts = metainfo.List("variations", "event_rec");
        for (TObject * obj: *ev_wgts) {
            auto name = dynamic_cast<TObjString*>(obj)->GetString();
            static int i = 0; ++i;    //       JEC GJW RJW GEW REW
            variations.push_back( Variation(name, 0, 0, 0, i) );
        }

        TList * JECs = metainfo.List("variations", "JECs");
        for (TObject * obj: *JECs) {
            auto name = dynamic_cast<TObjString*>(obj)->GetString();
            static int i = 0; ++i;    //       JEC GJW RJW GEW REW
            variations.push_back( Variation(name, i, 0, 0, 0) );
        }

        // TODO: automate
    }

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        for (const Variation& v: variations) {

            v.tmp->Reset();

            auto evWgt = ev->recWgts.at(v.iRecEvtWgt);
            if (isMC) evWgt *= ev->genWgts.at(v.iGenEvtWgt);

            vector<int> binIDs; // we save the indices of the filled bins to avoid trivial operations
            for (const RecJet& recjet: *recJets) {
                auto y  = recjet.AbsRap();
                if (y >= maxy) continue;
                auto pt = recjet.CorrPt(v.iJEC);
                if (pt < minpt || pt >= maxpt) continue;

                auto irecbin = v.rec->FindBin(pt);
                if (find(binIDs.begin(), binIDs.end(), irecbin) == binIDs.end()) binIDs.push_back(irecbin);

                auto jWgt = recjet.weights.at(v.iRecJetWgt);

                v.rec->Fill(pt, evWgt * jWgt);
                v.tmp->Fill(pt, evWgt * jWgt); // for cov
            }

            for (auto x: binIDs) for (auto y: binIDs) {
                double cCov = v.cov->GetBinContent(x,y),
                       cTmp = v.tmp->GetBinContent(x)*v.tmp->GetBinContent(y);
                v.cov->SetBinContent(x,y,cCov+cTmp);
            }
        }

        if (!isMC) continue;

        Matching<RecJet, GenJet> matching(*recJets);
        for (const GenJet& genjet: *genJets) {
            auto genpt = genjet.p4.Pt(),
                 geny = genjet.AbsRap();

            bool LowGenPt  = genpt < minpt,
                 HighGenPt = genpt >= maxpt,
                 HighGenY  =  geny >= maxy;
            bool goodGen = (!LowGenPt) && (!HighGenPt) && (!HighGenY);

            RecJet recjet = matching.HighestPt(genjet);
            bool noMatch = recjet.p4.Pt() > 13e3 /* 13 TeV */;

            for (const Variation& v: variations) {

                auto gEvW = ev->genWgts.at(v.iGenEvtWgt),
                     gJetW = genjet.weights.at(v.iGenJetWgt);

                if (goodGen) v.gen->Fill(genpt, gEvW * gJetW);

                if (noMatch) {
                    if (goodGen) v.missNoMatch->Fill(genpt, gEvW * gJetW);
                    continue;
                }

                auto rEvW = ev->recWgts.at(v.iRecEvtWgt),
                     rJetW = recjet.weights.at(v.iRecJetWgt);

                auto recpt = recjet.CorrPt(v.iJEC),
                     recy  = abs(recjet.Rapidity());

                bool LowRecPt  = recpt < minpt,
                     HighRecPt = recpt >= maxpt,
                     HighRecY  =  recy >= maxy;
                bool goodRec = (!LowRecPt) && (!HighRecPt) && (!HighRecY);

                if ( goodRec &&  goodGen) {    v.RM->Fill(genpt, recpt, gEvW * gJetW *      rEvW * rJetW  );
                                      v.missNoMatch->Fill(genpt,        gEvW * gJetW * (1 - rEvW * rJetW) ); }
           else if (!goodRec &&  goodGen) v.missOut->Fill(genpt,        gEvW * gJetW                      );
           else if ( goodRec && !goodGen) v.fakeOut->Fill(       recpt, gEvW * gJetW *      rEvW * rJetW  );
            }
            // TODO: try to use the underflow and overflow for matching out of the phase space...
        }

        for (RecJet& recjet: matching.mcands) {
            for (const Variation& v: variations) {

                double y  = abs(recjet.Rapidity());
                if (y >= maxy) continue;
                double pt = recjet.CorrPt(v.iJEC);
                if (pt < minpt || pt > maxpt) continue;

                auto evWgt =  ev->recWgts.at(v.iRecEvtWgt) * ev->genWgts.at(v.iGenEvtWgt); 
                auto jWgt = recjet.weights.at(v.iRecJetWgt);

                v.fakeNoMatch->Fill(pt, evWgt * jWgt);
            }
        }
    }

    fOut->cd();
    for (Variation& v: variations)
        v.Write(fOut.get());

    fOut->cd();
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Unfolding::InclusiveJet1D namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Fill histograms to run unfolding.", DT::syst | DT::split);

        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file"               );
        options(argc,argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Unfolding::InclusiveJet1D::getUnfHistPt(inputs, output, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
