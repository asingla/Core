#include "Core/Unfolding/interface/HTn.h"

#include "Math/VectorUtil.h"

#include <colours.h>
#include <exceptions.h>

using namespace std;
using namespace DAS::Unfolding::Rij;
namespace DE = Darwin::Exceptions;

HTnFiller::HTnFiller (const HTn& obs, TTreeReader& reader)
    : obs(obs)
    , genJets(initOptionalBranch<decltype(genJets)>(reader, "genJets"))
    , recJets(reader, "recJets")
    , ev(reader, "event")
{
}

void HTnFiller::match ()
{
    matched.reset();

    auto match = [this](size_t i, size_t j) {
        const FourVector& g = genJets->At(i).p4,
                          r = recJets.At(j).p4;
        using ROOT::Math::VectorUtil::DeltaR;
        auto DR = DeltaR(g, r);
        //cout << g << '\t' << r << '\t' << DR << '\t' << result << '\n';
        return DR < obs.maxDR;
    };

    // matching (swapping leading and subleading is allowed)
    matched = genJets->GetSize() > 1 && recJets.GetSize() > 1
              && (   (match(0,0) && match(1,1))
                  || (match(0,1) && match(1,0)) );
}

list<int> HTnFiller::fillRec (Variation& v)
{
    if (recJets.GetSize() < 2) return {};

    float evW = v.getCorrection(Event::RecWgtVar, ev->recWgts);
    if (obs.isMC) evW *= v.getCorrection(Event::GenWgtVar, ev->genWgts);

    auto Pt = [&v](const RecJet& j) { return v.getCorrP4(j).Pt(); };
    Dijet<RecJet> dijet(recJets, Pt);
    
    if (!dijet.update(v)) return {};

    auto ibin = obs.recBinning.GetGlobalBinNumber(dijet.HT, dijet.n);
    if (ibin == 0)
        cerr << red << dijet.HT << ' ' << dijet.n << '\n' << def;

    // TODO: gen weight?
    float djW = dijet.w;

    v.tmp->Fill(ibin, evW * djW);
    v.rec->Fill(ibin, evW * djW);

    return list<int>{ibin};
}

void HTnFiller::fillMC (Variation& v)
{
    if (!obs.isMC)
        BOOST_THROW_EXCEPTION( runtime_error(__func__ + " should only be called for MC"s) );

    // TODO: gen weight?

    float rEvW = v.getCorrection(Event::RecWgtVar, ev->recWgts),
          gEvW = v.getCorrection(Event::GenWgtVar, ev->genWgts);

    Dijet<GenJet> * gendijet = nullptr; // TODO: more modern C++ approach 
    Dijet<RecJet> * recdijet = nullptr; //       than raw pointers?

    auto igen = 0;
    bool goodGen = false;
    if (genJets->GetSize() > 1) {
        auto Pt = [](const GenJet& j) { return j.p4.Pt(); };
        gendijet = new Dijet<GenJet>(*genJets, Pt);
        goodGen = gendijet->update(v);
        if (goodGen) {
            igen = obs.genBinning.GetGlobalBinNumber(gendijet->HT, gendijet->n);
            v.gen->Fill(igen, gEvW * rEvW);
        }
    }

    auto irec = 0.;
    optional<double> rDjW;
    bool goodRec = false;
    if (recJets.GetSize() > 1) {
        auto Pt = [&v](const RecJet& j) { return v.getCorrP4(j).Pt(); };
        recdijet = new Dijet<RecJet>(recJets, Pt);
        goodRec = recdijet->update(v);
        if (goodRec) {
            irec = obs.recBinning.GetGlobalBinNumber(recdijet->HT, recdijet->n);
            rDjW = recdijet->w;
        }
    }

    if (*matched) {
        if      ( goodRec &&  goodGen) {    v.RM->Fill(igen, irec, gEvW *    rEvW * *rDjW ); 
                                   v.missNoMatch->Fill(igen,       gEvW * (1-rEvW * *rDjW)); }
        else if (!goodRec &&  goodGen) v.missOut->Fill(igen,       gEvW                   );
        else if ( goodRec && !goodGen) v.fakeOut->Fill(      irec, gEvW *    rEvW * *rDjW ); 
    }
    else {
        if (goodGen) v.missNoMatch->Fill(igen, gEvW               );
        if (goodRec) v.fakeNoMatch->Fill(irec, gEvW * rEvW * *rDjW);
    }

    if (recdijet != nullptr) delete recdijet;
    if (gendijet != nullptr) delete gendijet;
}

////////////////////////////////////////////////////////////////////////////////

HTn::HTn () :
    Observable(__FUNCTION__, "H_{T} spectra in bins of jet multiplicity")
{
    recBinning.AddAxis("HT", nRecHtBins,  recHt_edges.data(), false, false);
    recBinning.AddAxis("n" ,  nJetsBins, n_jets_edges.data(), false, false); // TODO: recBinning.AddAxis("n", 3, 1.5, 4.5, false, true);
    genBinning.AddAxis("HT", nGenHtBins,  genHt_edges.data(), false, false);
    genBinning.AddAxis("n" ,  nJetsBins, n_jets_edges.data(), false, false); // TODO: genBinning.AddAxis("n", 3, 1.5, 4.5, false, true);
}

unique_ptr<DAS::Unfolding::Filler> HTn::getFiller (TTreeReader& reader) const
{
    return make_unique<HTnFiller>(*this, reader);
}

void HTn::setLmatrix (const unique_ptr<TH1>& bias, unique_ptr<TH2>& L)
{
    for (int n = nJetsmin; n <= nJetsmax; ++n)
    for (int iHT = 1; iHT <= nGenHtBins; ++iHT) {

        auto HT = (genHt_edges.at(iHT-1) + genHt_edges.at(iHT)) / 2;
        int i = genBinning.GetGlobalBinNumber(HT, n);
        if (i == 0)
            BOOST_THROW_EXCEPTION( runtime_error("HT = "s + HT + " and n = "s + n
                                        + " do not correspond to any bin index"s) );

        // b(in)
        int               bUp     = i-nGenHtBins,
             bLeft = i-1, bCenter = i           , bRight  = i+1,
                          bDown   = i+nGenHtBins;

        // values (curvature regularisation)
        auto get = [&bias](int i) {
            auto content = bias->GetBinContent(i);
            if (content < 0)
                BOOST_THROW_EXCEPTION( DE::BadInput("Expecting only positive entries", bias) );
            return content > 0 ? 1./content : 0;
        };

        auto cUp    = get(bUp   ),
             cLeft  = get(bLeft ),
             cRight = get(bRight),
             cDown  = get(bDown );

        cout << setw(3) << n << setw(3) << iHT
             << setw(5) << bCenter << setw(15) << (cUp+cLeft+cRight+cDown)
             << setw(5) << bUp     << setw(15) << -cUp                   
             << setw(5) << bLeft   << setw(15) <<     -cLeft             
             << setw(5) << bRight  << setw(15) <<           -cRight      
             << setw(5) << bDown   << setw(15) <<                  -cDown << '\n';

        // filling L-matrix
                        L->SetBinContent(i, bCenter,  cUp+cLeft+cRight+cDown );
        if (cUp    > 0) L->SetBinContent(i, bUp    , -cUp                    );
        if (cLeft  > 0) L->SetBinContent(i, bLeft  ,     -cLeft              );
        if (cRight > 0) L->SetBinContent(i, bRight ,           -cRight       );
        if (cDown  > 0) L->SetBinContent(i, bDown  ,                  -cDown );
    }
    cout << flush;
}
