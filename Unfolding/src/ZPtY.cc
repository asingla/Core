#include "Core/Unfolding/interface/ZPtY.h"

#include "Core/CommonTools/interface/toolbox.h"
#include "Math/VectorUtil.h"
#include <exceptions.h>

using namespace std;
using namespace DAS;
using namespace DAS::Unfolding;
using namespace DAS::Unfolding::DrellYan;
namespace DE = Darwin::Exceptions;

namespace /* anonymous */ {

/**
 * Checks if muons pass the selection
 */
template<class Muon>
bool selected(const TTreeReaderArray<Muon>& muons, const Variation& v)
{
    if (muons.GetSize() < 2) return false;

    const auto p0 = v.getCorrP4(muons[0]),
               p1 = v.getCorrP4(muons[1]);
    if (p0.Pt() < minPt || p1.Pt() < minPt) return false;
    if (abs(p0.Eta()) > maxEta || abs(p1.Eta()) > maxEta) return false;

    const FourVector pZ = p0 + p1;
    return pZ.M() > minMll && pZ.M() < maxMll;
}

/**
 * Gets the bin number for the muons, or -1 if not selected
 */
template<class Muon>
int getBinNumber(const TUnfoldBinning& binning,
                 const TTreeReaderArray<Muon>& muons,
                 const Variation& v)
{
    if (!selected(muons, v)) return -1;

    const FourVector pZ = v.getCorrP4(muons[0]) + v.getCorrP4(muons[1]);
    return binning.GetGlobalBinNumber(pZ.Pt(), abs(Rapidity(pZ)));
}

/**
 * Gets the weights associated with the muons. They must pass the selection
 */
template<class Muon>
float getWeight(const TTreeReaderArray<Muon>& muons, const Variation& v)
{
    return v.getWeight(muons[0]) * v.getWeight(muons[1]);
}

} // anonymous namespace


ZPtYFiller::ZPtYFiller (const ZPtY& obs, TTreeReader& reader)
    : obs(obs)
    , genMuons(initOptionalBranch<decltype(genMuons)>(reader, "genMuons"))
    , recMuons(reader, "recMuons")
    , ev(reader, "event")
{}

list<int> ZPtYFiller::fillRec (Variation& v)
{
    float evW = v.getCorrection(Event::RecWgtVar, ev->recWgts);
    if (obs.isMC) evW *= v.getCorrection(Event::GenWgtVar, ev->genWgts);

    const auto bin = getBinNumber(obs.recBinning, recMuons, v);
    if (bin < 0) return {};

    const float muW = getWeight(recMuons, v);
    v.tmp->Fill(bin, evW * muW);
    v.rec->Fill(bin, evW * muW);

    return {bin};
}

void ZPtYFiller::fillMC (Variation& v)
{
    if (!obs.isMC)
        BOOST_THROW_EXCEPTION( runtime_error(__func__ + " should only be called for MC"s) );

    auto rEvW = v.getCorrection(Event::RecWgtVar, ev->recWgts),
         gEvW = v.getCorrection(Event::GenWgtVar, ev->genWgts);

    const int genBin = getBinNumber(obs.genBinning, *genMuons, v);
    const int recBin = getBinNumber(obs.recBinning, recMuons, v);

    const float genMuW = genBin >= 0 ? getWeight(*genMuons, v) : 0;
    const float recMuW = recBin >= 0 ? getWeight(recMuons, v) : 0;

    if (genBin >= 0) v.gen->Fill(genBin, gEvW * genMuW);

    if (genBin >= 0 && recBin >= 0) {
        // Good events
        v.RM     ->Fill(genBin, recBin, gEvW * genMuW *      rEvW * recMuW);        
        v.missOut->Fill(genBin,         gEvW * genMuW * (1 - rEvW * recMuW));        
    } else if (genBin >= 0 && recBin < 0)
        // Miss
        v.missOut->Fill(genBin,         gEvW * genMuW                     );
    else if (genBin < 0 && recBin >= 0)
        // Fake
        v.fakeOut->Fill(        recBin, gEvW *               rEvW * recMuW);
}

////////////////////////////////////////////////////////////////////////////////

ZPtY::ZPtY () :
    Observable(__FUNCTION__, "Z boson dsigma/dpT dy")
{
    recBinning.AddAxis("pt", nRecPtBins, recPtBins.data(), false, false);
    recBinning.AddAxis("y" ,     nYbins,     yBins.data(), false, false);
    genBinning.AddAxis("pt", nGenPtBins, genPtBins.data(), false, false);
    genBinning.AddAxis("y" ,     nYbins,     yBins.data(), false, false);
}

unique_ptr<DAS::Unfolding::Filler> ZPtY::getFiller (TTreeReader& reader) const
{
    return make_unique<ZPtYFiller>(*this, reader);
}

void ZPtY::setLmatrix (const unique_ptr<TH1>& bias, unique_ptr<TH2>& L)
{
    // TODO some docs... this is quite obscure
    // TODO helpers for common cases
    for (int iy = 1; iy <= nRecPtBins; ++iy)
    for (int ipt = 1; ipt <= nGenPtBins; ++ipt) {

        auto  y = (yBins.at(iy - 1) + yBins.at(iy)) / 2,
             pt = (genPtBins.at(ipt - 1) + genPtBins.at(ipt)) / 2;
        int i = genBinning.GetGlobalBinNumber(pt, y);
        if (i == 0)
            BOOST_THROW_EXCEPTION( logic_error(
                Form("pt = %f and y = %f do not correspond to any bin index", pt, y)) );

        // b(in)
        int               bUp     = i-nGenPtBins,
             bLeft = i-1, bCenter = i           , bRight  = i+1,
                          bDown   = i+nGenPtBins;

        // values (curvature regularisation)
        auto get = [&bias](int i) {
            auto content = bias->GetBinContent(i);
            if (content < 0)
                BOOST_THROW_EXCEPTION( DE::BadInput("Expecting only positive entries", bias) );
            return content > 0 ? 1./content : 0;
        };

        auto cUp    = get(bUp   ),
             cLeft  = get(bLeft ),
             cRight = get(bRight),
             cDown  = get(bDown );

        cout << setw(3) << iy << setw(3) << ipt
             << setw(5) << bCenter << setw(15) << (cUp+cLeft+cRight+cDown)
             << setw(5) << bUp     << setw(15) << -cUp                   
             << setw(5) << bLeft   << setw(15) <<     -cLeft             
             << setw(5) << bRight  << setw(15) <<           -cRight      
             << setw(5) << bDown   << setw(15) <<                  -cDown << '\n';

        // filling L-matrix
                        L->SetBinContent(i, bCenter,  cUp+cLeft+cRight+cDown );
        if (cUp    > 0) L->SetBinContent(i, bUp    , -cUp                    );
        if (cLeft  > 0) L->SetBinContent(i, bLeft  ,     -cLeft              );
        if (cRight > 0) L->SetBinContent(i, bRight ,           -cRight       );
        if (cDown  > 0) L->SetBinContent(i, bDown  ,                  -cDown );
    }
    cout << flush;
}
