#include "Core/Unfolding/interface/MjjYmax.h"

#include "Math/VectorUtil.h"

#include <colours.h>
#include <exceptions.h>

#include <TVectorT.h>

using namespace std;
using namespace DAS::Unfolding::DijetMass;
namespace DE = Darwin::Exceptions;

MjjYmaxFiller::MjjYmaxFiller (const MjjYmax& obs, TTreeReader& reader)
    : obs(obs)
    , genJets(initOptionalBranch<decltype(genJets)>(reader, "genJets"))
    , recJets(reader, "recJets")
    , ev(reader, "event")
{
}

void MjjYmaxFiller::match ()
{
    matched.reset();

    auto match = [this](size_t i, size_t j) {
        const FourVector& g = genJets->At(i).p4,
                          r = recJets.At(j).p4; 
        using ROOT::Math::VectorUtil::DeltaR;
        auto DR = DeltaR(g, r);
        //cout << g << '\t' << r << '\t' << DR << '\t' << result << '\n';
        return DR < obs.maxDR;
    };

    // matching (swapping leading and subleading is allowed)
    matched = genJets->GetSize() > 1 && recJets.GetSize() > 1
              && (   (match(0,0) && match(1,1))
                  || (match(0,1) && match(1,0)) );
}

list<int> MjjYmaxFiller::fillRec (Variation& v)
{
    if (recJets.GetSize() < 2) return {};

    float evW = v.getCorrection(Event::RecWgtVar, ev->recWgts);
    if (obs.isMC) evW *= v.getCorrection(Event::GenWgtVar, ev->genWgts);

    auto p4 = [&v](const RecJet& j) { return v.getCorrP4(j); };
    Dijet<RecJet> dijet(recJets.At(0), recJets.At(1), p4);

    if (!dijet.selection()) return {};

    auto ibin = obs.recBinning.GetGlobalBinNumber(dijet.Mjj(), dijet.Ymax);
    if (ibin == 0)
        cerr << red << dijet.Mjj() << ' ' << dijet.Ymax << '\n' << def;

    // TODO: gen weight?
    float djW = dijet.weight(v);

    v.tmp->Fill(ibin, evW * djW);
    v.rec->Fill(ibin, evW * djW);

    return list<int>{ibin};
}

void MjjYmaxFiller::fillMC (Variation& v)
{
    if (!obs.isMC)
        BOOST_THROW_EXCEPTION( runtime_error(__func__ + " should only be called for MC"s) );

    // TODO: gen weight?

    float gEvW = v.getCorrection(Event::RecWgtVar, ev->recWgts),
          rEvW = v.getCorrection(Event::GenWgtVar, ev->genWgts);

    Dijet<GenJet> * gendijet = nullptr; // TODO: more modern C++ approach 
    Dijet<RecJet> * recdijet = nullptr; //       than raw pointers?

    auto igen = 0;
    bool goodGen = false;
    if (genJets->GetSize() > 1) {
        auto p4 = [](const GenJet& j) { return j.p4; };
        gendijet = new Dijet<GenJet>(genJets->At(0), genJets->At(1), p4);
        TUnfoldBinning * genBinning = gendijet->Ymax < ymax_edges[ymax_transition]
                                ? &obs.genBinningCnt : &obs.genBinningFwd;
        igen = genBinning->GetGlobalBinNumber(gendijet->Mjj(), gendijet->Ymax);
        goodGen = gendijet->selection();
        if (goodGen) v.gen->Fill(igen, gEvW * rEvW);
    }

    auto irec = 0.;
    optional<double> rDjW;
    bool goodRec = false;
    if (recJets.GetSize() > 1) {
        auto p4 = [&v](const RecJet& j) { return v.getCorrP4(j); };
        recdijet = new Dijet<RecJet>(recJets.At(0), recJets.At(1), p4);
        irec = obs.recBinning. GetGlobalBinNumber(recdijet->Mjj(), recdijet->Ymax);
        rDjW = recdijet->weight(v);
        goodRec = recdijet->selection();
    }

    if (*matched) {
        if      ( goodRec &&  goodGen) {    v.RM->Fill(igen, irec, gEvW *    rEvW * *rDjW ); 
                                   v.missNoMatch->Fill(igen,       gEvW * (1-rEvW * *rDjW)); }
        else if (!goodRec &&  goodGen) v.missOut->Fill(igen,       gEvW                   );
        else if ( goodRec && !goodGen) v.fakeOut->Fill(      irec, gEvW *    rEvW * *rDjW ); 
    }
    else {
        if (goodGen) v.missNoMatch->Fill(igen, gEvW               );
        if (goodRec) v.fakeNoMatch->Fill(irec, gEvW * rEvW * *rDjW);
    }

    if (recdijet != nullptr) delete recdijet;
    if (gendijet != nullptr) delete gendijet;
}

////////////////////////////////////////////////////////////////////////////////

MjjYmax::MjjYmax () :
    Observable(__FUNCTION__, "Dijet mass double differential cross section"),
    genBinningCnt(""), genBinningFwd("")
{
    // TODO: simplify the logic

    // default gen binning (for central region)
    vector<double> genMjj_edgesCnt = Mjj_edges;
    nGenMjjBinsCnt = genMjj_edgesCnt.size()-1;

    vector<double> recMjj_edges, genMjj_edgesFwd;
    recMjj_edges.reserve(Mjj_edges.size()*2);
    genMjj_edgesFwd.reserve(Mjj_edges.size()/2);

    // rec binning with twice more bins
    for (size_t j = 0; j < Mjj_edges.size()-1; ++j) {
        double m = Mjj_edges.at(j),
               M = Mjj_edges.at(j+1);
        recMjj_edges.push_back(m);
        recMjj_edges.push_back((m+M)/2);
    }
    recMjj_edges.push_back(Mjj_edges.back());

    int nRecMjjBins = recMjj_edges.size()-1;

    // forward region with twice less bins
    for (size_t j = 0; j < Mjj_edges.size(); j += 2) {
        double m = Mjj_edges.at(j);
        genMjj_edgesFwd.push_back(m);
    }
    genMjj_edgesFwd.back() = genMjj_edgesCnt.back(); // just in case
    nGenMjjBinsFwd = genMjj_edgesFwd.size()-1;

    recBinning.AddAxis("Mjj", nRecMjjBins, recMjj_edges.data(),false,false);
    recBinning.AddAxis("y"  ,   nYmaxBins,   ymax_edges.data(),false,false);

    genBinningCnt.AddAxis("Mjj",            nGenMjjBinsCnt,       genMjj_edgesCnt.data(),false,false);
    genBinningCnt.AddAxis("y"  ,           ymax_transition,            ymax_edges.data(),false,false);
    genBinningFwd.AddAxis("Mjj",            nGenMjjBinsFwd,       genMjj_edgesFwd.data(),false,false);
    genBinningFwd.AddAxis("y"  , nYmaxBins-ymax_transition, &ymax_edges[ymax_transition],false,false);

    genBinning.AddBinning(&genBinningCnt);
    genBinning.AddBinning(&genBinningFwd);

    genBinningCnt.SetTitle(genBinning.GetTitle());
    genBinningFwd.SetTitle(genBinning.GetTitle());
}

unique_ptr<DAS::Unfolding::Filler> MjjYmax::getFiller (TTreeReader& reader) const
{
    return make_unique<MjjYmaxFiller>(*this, reader);
}

void MjjYmax::setLmatrix (const unique_ptr<TH1>& bias, unique_ptr<TH2>& L)
{
    for (size_t iy = 1; iy <= nYmaxBins; ++iy) {
        bool cnt = iy <= ymax_transition;
        TUnfoldBinning& genBinning = cnt ? genBinningCnt
                                         : genBinningFwd;
        const TVectorD * Mjj_edges = genBinning.GetDistributionBinning(0);
        int nMjjBins = Mjj_edges->GetNrows()-1;
        for (int iM = 1; iM <= nMjjBins; ++iM) {

            double Ymax = (  y_edges.at(iy-1) +   y_edges.at(iy)) / 2,
                   Mjj  = ((*Mjj_edges)[iM-1] + (*Mjj_edges)[iM]) / 2;
            int i = genBinning.GetGlobalBinNumber(Mjj, Ymax);
            if (i == 0)
                BOOST_THROW_EXCEPTION( runtime_error("Mjj = "s + Mjj + " and Ymax = "s + Ymax
                                        + " do not correspond to any bin index"s) );

            // b(in)
            int                 bUp     = i-nMjjBins, // TODO: this will not work at the transition
                 bLeft   = i-1, bCenter = i         , bRight  = i+1,
                                bDown   = i+nMjjBins; // TODO: this will not work at the transition

            // values (curvature regularisation)
            auto get = [&bias](int i) {
                double content = bias->GetBinContent(i);
                if (content < 0)
                    BOOST_THROW_EXCEPTION( DE::BadInput("Expecting only positive entries", bias) );
                return content > 0 ? 1./content : 0;
            };
            double cUp     = get(bUp   ),
                   cLeft   = get(bLeft ),
                   cRight  = get(bRight),
                   cDown   = get(bDown );

            cout << setw(3) << iy << setw(3) << iM
                 << setw(5) << bCenter << setw(15) << (cUp+cLeft+cRight+cDown)
                 << setw(5) << bUp     << setw(15) << -cUp                   
                 << setw(5) << bLeft   << setw(15) <<     -cLeft             
                 << setw(5) << bRight  << setw(15) <<           -cRight      
                 << setw(5) << bDown   << setw(15) <<                  -cDown << '\n';

            // filling L-matrix
                            L->SetBinContent(i, bCenter,  cUp+cLeft+cRight+cDown );
            if (cUp    > 0) L->SetBinContent(i, bUp    , -cUp                    );
            if (cLeft  > 0) L->SetBinContent(i, bLeft  ,     -cLeft              );
            if (cRight > 0) L->SetBinContent(i, bRight ,           -cRight       );
            if (cDown  > 0) L->SetBinContent(i, bDown  ,                  -cDown );
        }
    }
    cout << flush;
}
