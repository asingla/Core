# Unfolding

## Overview

The present package implements the binned unfolding with the [TUnfold package](https://www.desy.de/~sschmitt/tunfold.html) in a as generic as possible way, allowing the treatment of new observables with minimum effort. It relies on `TUnfoldBinning` to allow multidimensional and/or combined unfolding. The [recommendations from the statistics committee](https://twiki.cern.ch/twiki/bin/view/CMS/ScrecUnfolding) are followed as closely as possible. In principle, any matrix-based unfolding may be used with this input.

The unfolding is implemented in a rather general way to allow the unfolding of different observables, separately or within a combined fit. The input and output files of the different unfolding command try to keep a similar structure, typically corresponding to one `TDirectory` per variation.
- Use `getUnfHist` to fill the input histograms. A list of observables, corresponding to the namespace and class names, should be given as argument of the command; the class of all observables inherits from `DAS::Unfolding::Observable`. A `TDirectory` is created for each systematic variation, with histograms as defined in `DAS::Unfolding::Variation`.
- Use `unfold` to run the unfolding in a physics-agnostic way (i.e. it does not use any information from `TUnfoldBinning`). The unfolding is repeated for each variation in the identical way. Additional variations related to the unfolding are added in new `TDirectory`s.
- The `getUnfFinal` command is the very last command to run to map the histograms with bin IDs onto 1D histograms with physical axes.

For the additional commands, check the helper directly from the command.
 
### Tutorial

To get familiar with the procedure and with the code, a simple case of 1D unfolding of the inclusive jet spectrum can be run with the command `getUnfHistPt`. It does not use `TUnfoldBinning` nor `DAS::Unfolding::Observable` nor `DAS::Unfolding::Variation`, and makes the reading of the code easier for a newcomer. A simple call of `unfold` on the output of `getUnfHistPt` is sufficient to obtain unfolded spectra directly with a physical axis.

To add a new observable, write a new class inheriting from `DAS::Unfolding::Observable` and place it into `interface` (header) and `src` (main file). Decide on a namespace (a generic name) and a class name (corresponding to the variables themselves). Include the header in all executables in `bins`. Implement all the functions of the abstract (check their purposes directly in the file).

To fine-tune existing observables, you may play with the binning, usually defined in the headers in `interface`, and, in case of need of regularisation, the L-matrix, defined in the respective files in `src`.
