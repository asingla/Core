#ifndef DAS_UNFOLDING_HTN
#define DAS_UNFOLDING_HTN

#include <vector>
#include <list>
#include <algorithm>
#include <optional>

#include <TUnfoldBinning.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>
#include <TH1.h>
#include <TH2.h>

#if !defined(__CLING__) || defined(__ROOTCLING__)
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Unfolding/interface/Observable.h"
#endif

namespace DAS::Unfolding::Rij {

static const std::vector<double> genHt_edges  = { /*100,  150,  */200,  250,  300,  360,  430,  510,  600,  700,  800,  920, 1060, 
                                                 1220, 1400, 1600, 1840, 2100, 2400, 2740, 3140, 3590, 4120, 4700, 5500 },
                                 recHt_edges  = { /*100,  125,  150,  175,*/  200,  225,  250,  275,  300,  330,  360,  390,  430,  470,  510,  550,
                                                  600,  650,  700,  750,  800,  860,  920,  980, 1060, 1140, 1220, 1300, 1400, 1500, 1600, 1720, 
                                                 1840, 1960, 2100, 2240, 2400, 2560, 2740, 2940, 3140, 3340, 3590, 3840, 4120, 4400, 4700, 5050, 5500 },
                                 n_jets_edges = { 1.5, 2.5, 3.5, 4.5, 5.5 };

static const int nRecHtBins = recHt_edges.size()-1,
                 nGenHtBins = genHt_edges.size()-1,
                 nJetsBins = n_jets_edges.size()-1;

static const float Htmin    = recHt_edges.front(),
                   Htmax    = recHt_edges.back(),
                   nJetsmin = n_jets_edges.front()+0.5,
                   nJetsmax = n_jets_edges.back()-0.5,
                   ptmin    = 150 /* GeV */,
                   ymax     = 2.5;

#if !defined(__CLING__) || defined(__ROOTCLING__)
////////////////////////////////////////////////////////////////////////////////
/// Workflow on event handling:
/// Require leading and subleading jets to pass the pt and y cuts.
/// If either the leading or subleading jet fails the cuts we discard the event.
/// For remaining jets we keep only those that pass the pt and y cuts.
/// Event multiplicity is then evaluated by only those jets that have passed the above criteria.
///
/// Histogram fill cases according to multiplicity:
///  - Exclusive 2 jets
///  - Exclusive 3 jets
///  - Exclusive 4 jets
///  - Inclusive 5 jets
template<typename Jet> struct Dijet {
    const TTreeReaderArray<Jet>& jets;
    std::function<float(const Jet&)> Pt;
    float HT, n, w;

    bool update (const Variation& v)
    {
        if (jets.GetSize() < 2) return false;

        HT = ( Pt(jets.At(0)) + Pt(jets.At(1)) )/2;

        n = 0;
        w = 1;
        for (auto& jet: jets) { // Count which jets pass the pt and y cuts
            if (jet.AbsRap() >= ymax || Pt(jet) < ptmin ) continue;
            ++n;
            w *= v.getWeight(jet);
        }
        n = std::min(n, nJetsmax);

        return Pt(jets.At(0)) >= ptmin && Pt(jets.At(1)) >= ptmin
            && jets.At(0).AbsRap() < ymax && jets.At(1).AbsRap() < ymax
            && HT >= Htmin && HT < Htmax;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    Dijet (const TTreeReaderArray<Jet>& Jets, std::function<float(const Jet&)> pt) :
        jets(Jets), Pt(pt)
    { }
};

struct HTn final : public Observable {
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    HTn ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable
    std::unique_ptr<Filler> getFiller (TTreeReader& reader) const override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::getLmatrix`
    void setLmatrix (const std::unique_ptr<TH1>&, std::unique_ptr<TH2>&) override;
};

struct HTnFiller final : public Filler {
    HTn obs; ///< Backreference to the observable

    std::optional<TTreeReaderArray<GenJet>> genJets;
    TTreeReaderArray<RecJet> recJets;
    TTreeReaderValue<Event> ev;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    HTnFiller (const HTn& obs, TTreeReader& reader);

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillRec`
    std::list<int> fillRec (Variation&) override;

    std::vector<std::pair<GenJet,RecJet>> matches;
    std::vector<GenJet> misses;
    std::vector<RecJet> fakes;

    std::optional<bool> matched;

    ////////////////////////////////////////////////////////////////////////////////
    /// Match the two pairs of leading jets (if any) and set `matched` member.
    void match () override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillMC`
    void fillMC (Variation&) override;
};
#endif

} // end of DAS::Unfolding::Rij namespace
#endif
