#pragma once

#include <iostream>
#include <list>
#include <memory>
#include <optional>

#include <TString.h>
#include <TTreeReader.h>
#include <TUnfoldBinning.h>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/Unfolding/interface/Variation.h"

class TH1;
class TH2;

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Utility function to init an optional TTreeReaderArray/TTreeReaderValue.
/// Example:
/// ~~~{.cpp}
/// struct A
/// {
///     std::optional<TTreeReaderValue<int>> value;
///     A(TTreeReader& reader)
///         : value(initOptionalBranch<decltype(value)>(reader, "branchName"))
///     {}
/// };
/// ~~~
///
/// \copyright
/// Originally written for the [Shears](https://gitlab.cern.ch/shears/shears)
/// framework.
template<class T>
auto initOptionalBranch(TTreeReader& reader, const char *name)
{
     return reader.GetTree()->GetBranch(name) != nullptr
         ? std::make_optional<typename T::value_type>(reader, name)
         : std::nullopt;
}

////////////////////////////////////////////////////////////////////////////////
/// Fills histograms for an observable
///
/// Idea:
/// - use pointers to this abstract class to define observables in daughter classes
/// - the fill functions are generic, and should be implemented in daughter classes
/// Goal: factorise the filling of the input histogram to the unfolding in an
///       observable-agnostic way, hence avoiding multiple executables.
///
/// Subclasses should use TTreeReaderValue and TTreeReaderArray to read branches.
struct Filler {
    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor.
    virtual ~Filler () = default;

    ////////////////////////////////////////////////////////////////////////////////
    /// For a given variation, fills the detector level distribution, as well as 
    /// a temporary histogram (reset at each event) to calculate the covariance
    /// matrix later on. The bin IDs are returned to avoid looping over many empty
    /// bins.
    virtual std::list<int> fillRec (Variation&) = 0;

    ////////////////////////////////////////////////////////////////////////////////
    /// Implementation of matching algorithm.
    virtual void match () = 0;

    ////////////////////////////////////////////////////////////////////////////////
    /// Fill RM, gen, miss, fake histograms for a given variation.
    virtual void fillMC (Variation&) = 0;
};

////////////////////////////////////////////////////////////////////////////////
/// Represents an observable that can be unfolded
///
/// Idea:
/// - use pointers to this abstract class to define observables in daughter classes
/// - this class handles everything that doesn't access event data (e.g. the
///   binning)
/// - it also gives access to a \ref Filler through \ref getFiller()
///
/// This class is used repeatedly by several executables related to the unfolding
/// (nearly all, to the notable exception of `unfold` itself), as well as in macros
/// used for plotting.
struct Observable {

    static bool isMC; //!< flag from metainfo
    // TODO specific to jet analyses
    static float maxDR; //!< max Delta R

    // The following objects may be used either directly in `getUnfHist` command
    // OR to define a more global binning in case of combined unfolding of several
    // observables.
    TUnfoldBinning recBinning, //!< detector-level binning
                   genBinning; //!< hadron-level binning

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable
    virtual std::unique_ptr<Filler> getFiller (TTreeReader& reader) const = 0;

    ////////////////////////////////////////////////////////////////////////////////
    /// Construct regularisation matrix
    virtual void setLmatrix (const std::unique_ptr<TH1>&, //!< bias
                             std::unique_ptr<TH2>& //!< (global) L matrix
                            ) = 0;

    virtual ~Observable () = default;
protected:

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    Observable (const char *, const char *);
};

////////////////////////////////////////////////////////////////////////////////
/// Get the observables to unfold
std::vector<Observable *> GetObservables // TODO: smart pointer?
                    (boost::property_tree::ptree //!< expect labels
                     );

} // end of namespace DAS::Unfolding
