#ifndef DAS_UNFOLDING_MJJYMAX
#define DAS_UNFOLDING_MJJYMAX

#include <list>
#include <vector>
#include <optional>
#include <functional>

#include <TUnfoldBinning.h>
#include <TTreeReaderArray.h>
#include <TH1.h>
#include <TH2.h>

#if !defined(__CLING__) || defined(__ROOTCLING__)
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Unfolding/interface/Observable.h"
#endif

namespace DAS::Unfolding::DijetMass {

static const std::vector<double> Mjj_edges {/*160,*/  200,  249,  306,  372,  449,  539, 641,  756,  887, 1029, 1187, 1361, 1556, 1769, 2008, 2273, 2572, 2915, 3306, 3754, 4244, 4805, 5374, 6094, 6908, 7861, 8929, 10050 },
             ymax_edges{0., 0.5, 1.0, 1.5, 2.0, 2.5, 3.0};

static const size_t ymax_transition = 3,
                    nYmaxBins = ymax_edges.size() - 1;

static const auto minMjj = Mjj_edges.front(),
                  maxMjj = Mjj_edges.back(),
                  maxy = ymax_edges.back();

#if !defined(__CLING__) || defined(__ROOTCLING__)
////////////////////////////////////////////////////////////////////////////////
/// Dijet object and important properties,
/// to be used similarly as a `FourVector`.
template<typename Jet> struct Dijet {
    const Jet& jet0, jet1;
    std::function<FourVector(const Jet&)> p4;
    const float Ymax;

    inline float Mjj ()
    {
        FourVector rec0 = p4(jet0),
                   rec1 = p4(jet1);

        return sqrt(  pow(rec0.E ()+rec1.E (),2)
                    - pow(rec0.Px()+rec1.Px(),2)
                    - pow(rec0.Py()+rec1.Py(),2)
                    - pow(rec0.Pz()+rec1.Pz(),2));
    }

    inline float weight (const Variation& v)
    {
        return v.getWeight(jet0) * v.getWeight(jet1);
    }

    inline bool selection ()
    {
        auto mjj = Mjj();
        return p4(jet0).Pt() >= 200. && p4(jet1).Pt() >= 150. // GeV
            && jet0.AbsRap() < maxy && jet1.AbsRap() < maxy
            && mjj >= minMjj && mjj < maxMjj;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    Dijet (const Jet& j0, const Jet& j1, std::function<FourVector(const Jet&)> P4) :
        jet0(j0), jet1(j1), p4(P4), Ymax(std::max(j0.AbsRap(),j1.AbsRap()))
    { }
};

struct MjjYmax final : public Observable {
    int nGenMjjBinsFwd, nGenMjjBinsCnt;

    TUnfoldBinning genBinningCnt, //!< barrel region
                   genBinningFwd; //!< endcap region

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    MjjYmax ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable
    std::unique_ptr<Filler> getFiller (TTreeReader& reader) const override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::getLmatrix`
    ///
    /// TODO: fix
    void setLmatrix (const std::unique_ptr<TH1>&, std::unique_ptr<TH2>&) override;
};

struct MjjYmaxFiller final : public Filler {
    MjjYmax obs; ///< Backreference to the observable

    std::optional<TTreeReaderArray<GenJet>> genJets;
    TTreeReaderArray<RecJet> recJets;
    TTreeReaderValue<Event> ev;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    MjjYmaxFiller (const MjjYmax &obs, TTreeReader& reader);

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::fillRec`
    std::list<int> fillRec (Variation&) override;

    std::optional<bool> matched;

    ////////////////////////////////////////////////////////////////////////////////
    /// Match the two pairs of leading jets (if any) and set `matched` member.
    void match () override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::fillMC`
    void fillMC (Variation&) override;
};
#endif

} // end of DAS::Unfolding::DijetMass namespace
#endif
